# Teoria das categorias para programadores

## Tabela de conteúdos
[[_TOC_]]

## Parte um
### Categoria: A essência da composição
>Fiquei impressionado com a resposta positiva ao meu post anterior, o Prefácio à Teoria das Categorias para Programadores. Ao mesmo tempo, isso me assustou muito, porque percebi as grandes expectativas que as pessoas depositavam em mim. Eu temo que não importa o que eu escreva, muitos leitores ficarão desapontados. Alguns leitores gostariam que o livro fosse mais prático, outros mais abstrato. Alguns odeiam C++ e gostariam de todos os exemplos em Haskell, outros odeiam Haskell e exigem exemplos em Java. E eu sei que o ritmo de exposição será lento demais para alguns e rápido demais para outros. Este não será o livro perfeito. Será um compromisso. Tudo o que posso esperar é poder compartilhar um pouco dos meus momentos aha! com meus leitores. Vamos começar com o básico.

Uma categoria é um conceito embaraçosamente simples. Uma categoria consiste em objetos e setas que vão entre eles. É por isso que as categorias são tão fáceis de representar fotograficamente. Um objeto pode ser desenhado como um círculo ou um ponto, e uma seta... é uma seta. (Apenas para variar, ocasionalmente desenharei objetos como porquinhos e setas como fogos de artifício.) Mas a essência de uma categoria é a composição. Ou, se preferir, a essência da composição é uma categoria. As setas compõem, então se você tem uma seta do objeto A para o objeto B, e outra seta do objeto B para o objeto C, então deve haver uma seta - sua composição - que vai de A para C.

![](images/image_1.jpg)

<em>Em uma categoria, se há uma seta indo de A para B e uma seta indo de B para C, então também deve haver uma seta direta de A para C que é sua composição. Este diagrama não é uma categoria completa porque está faltando morfismos de identidade (veja mais tarde).</em>

#### Setas como funções
Isso já é um absurdo abstrato demais? Não se desespere. Vamos falar de concretos. Pense nas setas, também chamadas de morfismos, como funções. Você tem uma função `f` que recebe um argumento do tipo A e retorna um B. Você tem outra função `g` que recebe um B e retorna um C. Você pode compô-los passando o resultado de f para g. Você acabou de definir uma nova função que recebe um A e retorna um C.

Em matemática, essa composição é denotada por um pequeno círculo entre as funções: `g∘f`. Observe a ordem de composição da direita para a esquerda. Para algumas pessoas, isso é confuso. Você pode estar familiarizado com a notação de pipe no Unix, como em:
```bash
lsof | grep Chrome
```

ou a divisa >> em F#, que vão da esquerda para a direita. Mas em matemática e em Haskell, funções compõem da direita para a esquerda. Ajuda se você ler `g∘f` como "g depois de f."

Vamos deixar isso ainda mais explícito escrevendo algum código C. Temos uma função `f` que recebe um argumento do tipo A e retorna um valor do tipo B:
```c
B f(A a);
```

e outro:
```c
C g(B b);
```

Sua composição é:
```c
C g_depois_f(A a)
{
    return g(f(a));
}
```

Aqui, novamente, você vê a composição da direita para a esquerda: `g(f(a));` desta vez em C.

Eu gostaria de poder dizer a você que há um modelo na biblioteca padrão C++ que pega duas funções e retorna sua composição, mas não há uma. Então, vamos tentar um pouco de Haskell para variar. Aqui está a declaração de uma função de A para B:
```hs
f :: A -> B
```

De forma similar:
```hs
g :: B -> C
```

Sua composição é:
```hs
g . f
```

Depois de ver como as coisas são simples em Haskell, a incapacidade de expressar conceitos funcionais diretos em C++ é um pouco embaraçosa. Na verdade, Haskell permitirá que você use caracteres Unicode para que você possa escrever composições como:
```hs
g ∘ f
```

Você pode até usar dois pontos e setas Unicode:
```hs
f ∷ A → B
```

Então, aqui está a primeira lição de Haskell: dois pontos duplos significa "tem o tipo de..." Um tipo de função é criado inserindo uma seta entre dois tipos. Você compõe duas funções inserindo um ponto final entre elas (ou um círculo Unicode).

#### Propriedades da composição
Existem duas propriedades extremamente importantes que a composição em qualquer categoria deve satisfazer.

1. A composição é associativa. Se você tiver três morfismos, `f`, `g` e `h`, que podem ser compostos (ou seja, seus objetos correspondem de ponta a ponta), você não precisa de parênteses para compô-los. Em notação matemática, isso é expresso como:
```
h ∘ (g ∘ f) = (h ∘ g) ∘ f = h ∘ g ∘ f
```

Em (pseudo) Haskell:
```hs
f :: A -> B
g :: B -> C
h :: C -> D
h . (g . f) == (h . g) . f == h . g . f
```

(Eu disse "pseudo", porque a igualdade não é definida para funções.)

A associatividade é bastante óbvia ao lidar com funções, mas pode não ser tão óbvia em outras categorias.

2. Para cada objeto A existe uma seta que é uma unidade de composição. Esta seta circula do objeto para si mesma. Ser uma unidade de composição significa que, quando composto com qualquer seta que comece em A ou termine em A, respectivamente, ele retorna a mesma seta. A seta de unidade para o objeto A é chamada idₐ (identidade em A). Em notação matemática, se `f` vai de A para B, então

$`f \circ id_A = f`$

e

$`id_B \circ f = f`$

Ao lidar com funções, a seta de identidade é implementada como a função de identidade que apenas retorna seu argumento. A implementação é a mesma para todos os tipos, o que significa que essa função é universalmente polimórfica. Em C++, podemos defini-lo como um template:
```cpp
template<class T>
T id(T x) {
    return x;
}
```

Claro, em C++ nada é tão simples, porque você deve levar em conta não apenas o que está passando, mas também como (isto é, por valor, por referência, por referência const, por move e assim por diante).

Em Haskell, a função de identidade faz parte da biblioteca padrão (chamada Prelude). Aqui está sua declaração e definição:
```hs
id :: a -> a
id x = x
```

Como você pode ver, as funções polimórficas em Haskell são moleza. Na declaração, você apenas substitui o tipo por uma variável de tipo. Aqui está o truque: nomes de tipos concretos sempre começam com uma letra maiúscula, nomes de variáveis ​​de tipo começam com uma letra minúscula. Portanto, aqui `a` representa todos os tipos.

As definições de função em Haskell consistem no nome da função seguido por parâmetros formais - aqui apenas um, `x`. O corpo da função segue o sinal de igual. Essa concisão costuma ser chocante para os recém-chegados, mas você verá rapidamente que faz todo o sentido. A definição e a chamada de função são o pão com manteiga da programação funcional, portanto, sua sintaxe é reduzida ao mínimo. Não só não há parênteses em torno da lista de argumentos, mas também não há vírgulas entre os argumentos (você verá isso mais tarde, quando definirmos funções de vários argumentos).

O corpo de uma função é sempre uma expressão - não há instruções nas funções. O resultado de uma função é esta expressão - aqui, apenas `x`.

Isso conclui nossa segunda lição de Haskell.

As condições de identidade podem ser escritas (novamente, em pseudo-Haskell) como:
```hs
f . id == f
id . f == f
```

Você pode estar se perguntando: Por que alguém se importaria com a função de identidade - uma função que não faz nada? Então, novamente, por que nos preocupamos com o número zero? Zero é um símbolo para nada. Os antigos romanos tinham um sistema numérico sem zero e foram capazes de construir estradas e aquedutos excelentes, alguns dos quais sobrevivem até hoje.

Valores neutros como zero ou id são extremamente úteis ao trabalhar com variáveis ​​simbólicas. É por isso que os romanos não eram muito bons em álgebra, enquanto os árabes e persas, que estavam familiarizados com o conceito de zero, eram. Portanto, a função de identidade se torna muito útil como um argumento para - ou um retorno de - uma função de ordem superior. Funções de ordem superior são o que torna possível a manipulação simbólica de funções. Eles são a álgebra de funções.

Para resumir: uma categoria consiste em objetos e setas (morfismos). As setas podem ser compostas e a composição é associativa. Cada objeto possui uma seta de identidade que serve como uma unidade sob composição.

#### Composição é a essência da programação
Os programadores funcionais têm uma maneira peculiar de abordar problemas. Eles começam fazendo perguntas muito zen. Por exemplo, ao projetar um programa interativo, eles perguntariam: O que é interação? Ao implementar o Jogo da Vida de Conway, eles provavelmente ponderariam sobre o significado da vida. Com esse espírito, vou perguntar: O que é programação? No nível mais básico, a programação consiste em dizer ao computador o que fazer. “Pegue o conteúdo do endereço de memória x e adicione-o ao conteúdo do registro EAX.” Mas mesmo quando programamos em conjunto, as instruções que damos ao computador são uma expressão de algo mais significativo. Estamos resolvendo um problema não trivial (se fosse trivial, não precisaríamos da ajuda do computador). E como resolvemos problemas? Decompomos problemas maiores em problemas menores. Se os problemas menores ainda forem muito grandes, nós os decomporemos ainda mais e assim por diante. Finalmente, escrevemos um código que resolve todos os pequenos problemas. E então vem a essência da programação: compomos essas partes de código para criar soluções para problemas maiores. A decomposição não faria sentido se não fôssemos capazes de juntar as peças novamente.

Este processo de decomposição e recomposição hierárquica não nos é imposto pelos computadores. Ele reflete as limitações da mente humana. Nossos cérebros só podem lidar com um pequeno número de conceitos de cada vez. Um dos artigos mais citados em psicologia, The Magical Number Seven, Plus or Minus Two, postulou que só podemos manter 7 ± 2 “pedaços” de informação em nossas mentes. Os detalhes de nossa compreensão da memória humana de curto prazo podem estar mudando, mas sabemos com certeza que é limitado. O ponto principal é que não podemos lidar com a sopa de objetos ou com o espaguete de código. Precisamos de estrutura não porque programas bem estruturados sejam agradáveis ​​de se olhar, mas porque, caso contrário, nosso cérebro não pode processá-los de forma eficiente. Muitas vezes descrevemos alguma parte do código como elegante ou bonita, mas o que realmente queremos dizer é que é fácil de processar por nossas limitadas mentes humanas. O código elegante cria pedaços que têm o tamanho certo e vêm no número certo para que nosso sistema digestivo mental os assimile.

Então, quais são os pedaços certos para a composição dos programas? Sua área de superfície deve aumentar mais lentamente do que seu volume. (Gosto dessa analogia por causa da intuição de que a área de superfície de um objeto geométrico cresce com o quadrado de seu tamanho - mais lento do que o volume, que cresce com o cubo de seu tamanho.) A área de superfície é a informação de que precisamos para fazer o pedido para compor pedaços. O volume é a informação de que precisamos para implementá-los. A ideia é que, uma vez que um pedaço seja implementado, podemos esquecer os detalhes de sua implementação e nos concentrar em como ele interage com outros pedaços. Na programação orientada a objetos, a superfície é a declaração de classe do objeto ou sua interface abstrata. Na programação funcional, é a declaração de uma função. (Estou simplificando um pouco as coisas, mas esse é o ponto principal.)

A teoria das categorias é extrema no sentido de que nos desencoraja ativamente de olhar para dentro dos objetos. Um objeto na teoria das categorias é uma entidade nebulosa abstrata. Tudo o que você pode saber sobre isso é como ele se relaciona com outros objetos - como se conecta a eles usando setas. É assim que os motores de busca da Internet classificam os sites, analisando os links de entrada e saída (exceto quando eles trapaceiam). Na programação orientada a objetos, um objeto idealizado só é visível por meio de sua interface abstrata (superfície pura, sem volume), com métodos desempenhando o papel de setas. No momento em que você precisa se aprofundar na implementação do objeto para entender como compô-lo com outros objetos, você perde as vantagens do seu paradigma de programação.

#### Desafios
1. Implemente, da melhor maneira possível, a função de identidade em sua linguagem favorita (ou o segunda favorita, se sua linguagem favorita for Haskell).
2. Implemente a função de composição em sua linguagem favorita. Ele recebe duas funções como argumentos e retorna uma função que é sua composição.
3. Escreva um programa que tente testar se sua função de composição respeita a identidade.
4. A rede mundial de computadores é uma categoria em algum sentido? Os links são morfismos?
5. O Facebook é uma categoria, com pessoas como objetos e amizades como morfismos?
6. Quando um gráfico direcionado é uma categoria?

### Tipos e funções
A categoria de tipos e funções desempenha um papel importante na programação, então vamos falar sobre o que são tipos e por que precisamos deles.

#### Quem precisa de tipos?
Parece haver alguma controvérsia sobre as vantagens da tipagem estática vs dinâmica e forte vs fraca. Deixe-me ilustrar essas escolhas com um experimento mental. Imagine milhões de macacos em teclados de computador, batendo alegremente em teclas aleatórias, produzindo programas, compilando e executando-os.

![](images/image_2.jpg)

Com a linguagem de máquina, qualquer combinação de bytes produzida por macacos seria aceita e executada. Mas com linguagens de nível superior, apreciamos o fato de que um compilador é capaz de detectar erros lexicais e gramaticais. Muitos macacos ficarão sem bananas, mas os programas restantes terão uma chance melhor de serem úteis. A verificação de tipo fornece mais uma barreira contra programas sem sentido. Além disso, enquanto em uma linguagem tipada dinamicamente, incompatibilidades de tipo seriam descobertas em tempo de execução, em linguagens fortemente tipadas estaticamente verificadas as incompatibilidades de tipo são descobertas em tempo de compilação, eliminando muitos programas incorretos antes que eles tenham a chance de serem executados.

Portanto, a questão é: queremos deixar os macacos felizes ou queremos produzir programas corretos?

O objetivo usual no experimento mental dos macacos digitadores é a produção das obras completas de Shakespeare. Ter um corretor ortográfico e um corretor gramatical aumentaria drasticamente as chances. O análogo de um verificador de tipos iria ainda mais longe, certificando-se de que, uma vez que Romeu seja declarado um ser humano, ele não germine folhas ou prenda fótons em seu poderoso campo gravitacional.

#### Tipos são sobre composibilidade
A teoria das categorias trata da composição de setas. Mas nem todas duas setas podem ser compostas. O objeto de destino de uma seta deve ser o mesmo que o objeto de origem da próxima seta. Na programação, passamos os resultados de uma função para outra. O programa não funcionará se a função de destino não for capaz de interpretar corretamente os dados produzidos pela função de origem. As duas extremidades devem se ajustar para que a composição funcione. Quanto mais forte for o sistema de tipos da linguagem, melhor essa correspondência pode ser descrita e verificada mecanicamente.

O único argumento sério que ouço contra a forte verificação de tipo estático é que ela pode eliminar alguns programas semanticamente corretos. Na prática, isso acontece extremamente raramente e, em qualquer caso, cada linguagem fornece algum tipo de backdoor para contornar o sistema de tipos quando isso é realmente necessário. Até Haskell tem `unsafeCoerce`. Mas esses dispositivos devem ser usados ​​com cautela. O personagem de Franz Kafka, Gregor Samsa, quebra o sistema de tipos quando ele se metamorfoseia em um inseto gigante, e todos nós sabemos como isso termina.

Outro argumento que ouço muito é que lidar com tipos impõe muito peso ao programador. Pude simpatizar com esse sentimento depois de ter que escrever algumas declarações de iteradores em C++ sozinho, exceto que existe uma tecnologia chamada inferência de tipo que permite ao compilador deduzir a maioria dos tipos do contexto em que são usados. Em C++, agora você pode declarar uma variável como `auto` e deixar o compilador descobrir seu tipo.

Em Haskell, exceto em raras ocasiões, as anotações de tipo são puramente opcionais. Os programadores tendem a usá-los de qualquer maneira, porque eles podem dizer muito sobre a semântica do código e tornam os erros de compilação mais fáceis de entender. É uma prática comum em Haskell iniciar um projeto desenhando os tipos. Posteriormente, as anotações de tipo conduzem a implementação e tornam-se comentários impostos pelo compilador.

A tipagem estática forte costuma ser usada como desculpa para não testar o código. Às vezes, você pode ouvir os programadores de Haskell dizendo: “Se compilar, deve estar correto”. Obviamente, não há garantia de que um programa de tipo correto seja correto no sentido de produzir a saída correta. O resultado dessa atitude arrogante é que em vários estudos Haskell não veio tão à frente do pacote em qualidade de código como seria de se esperar. Parece que, no cenário comercial, a pressão para corrigir bugs é aplicada apenas até um determinado nível de qualidade, que tem tudo a ver com a economia do desenvolvimento de software e a tolerância do usuário final, e muito pouco a ver com o linguagem ou metodologia de programação. Um critério melhor seria medir quantos projetos estão atrasados ​​ou são entregues com funcionalidade drasticamente reduzida.

Quanto ao argumento de que o teste de unidade pode substituir a tipagem forte, considere a prática comum de refatoração em linguagens fortemente tipadas: alterar o tipo de um argumento de uma função específica. Em uma linguagem fortemente tipada, é suficiente modificar a declaração dessa função e, em seguida, corrigir todas as quebras de compilação. Em uma linguagem de tipagem fraca, o fato de que uma função agora espera dados diferentes não pode ser propagado para a rede de chamadas. O teste de unidade pode detectar algumas incompatibilidades, mas o teste é quase sempre um processo probabilístico, em vez de determinístico. O teste é um substituto insatisfatório para a prova.

#### O que são tipos?
A intuição mais simples para tipos é que eles são conjuntos de valores. O tipo `Bool` (lembre-se, tipos concretos começam com uma letra maiúscula em Haskell) é um conjunto de dois elementos `True` e `False`. O tipo `Char` é um conjunto de todos os caracteres Unicode como 'a' ou 'ą'.

Os conjuntos podem ser finitos ou infinitos. O tipo `String`, que é sinônimo de lista de `Char`, é um exemplo de conjunto infinito.

Quando declaramos `x` como um `Integer`:
```hs
x :: Integer
```

estamos dizendo que é um elemento do conjunto de inteiros. `Integer` em Haskell é um conjunto infinito e pode ser usado para fazer aritmética de precisão arbitrária. Também existe um `Int` de conjunto finito que corresponde ao tipo de máquina, assim como o int do C++.

Existem algumas sutilezas que tornam essa identificação de tipos e conjuntos complicada. Existem problemas com funções polimórficas que envolvem definições circulares e com o fato de que você não pode ter um conjunto de todos os conjuntos; mas, como prometi, não vou ser um defensor da matemática. O bom é que existe uma categoria de conjuntos, que é chamada de `Set`, e vamos apenas trabalhar com ela. Em `Set`, objetos são conjuntos e morfismos (setas) são funções.

`Set` é uma categoria muito especial, porque podemos realmente espiar dentro de seus objetos e obter muitas intuições ao fazer isso. Por exemplo, sabemos que um conjunto vazio não possui elementos. Sabemos que existem conjuntos especiais de um elemento. Sabemos que as funções mapeiam elementos de um conjunto para elementos de outro conjunto. Eles podem mapear dois elementos para um, mas não um elemento para dois. Sabemos que uma função de identidade mapeia cada elemento de um conjunto para si mesmo, e assim por diante. O plano é esquecer gradualmente todas essas informações e, em vez disso, expressar todas essas noções em termos puramente categóricos, ou seja, em termos de objetos e setas.

No mundo ideal, diríamos apenas que os tipos Haskell são conjuntos e as funções Haskell são funções matemáticas entre conjuntos. Há apenas um pequeno problema: uma função matemática não executa nenhum código - ela apenas sabe a resposta. Uma função Haskell deve calcular a resposta. Não é um problema se a resposta pode ser obtida em um número finito de etapas - por maior que seja esse número. Mas existem alguns cálculos que envolvem recursão e podem nunca terminar. Não podemos simplesmente banir funções não-terminadas de Haskell porque distinguir entre funções de terminação e não-terminação é indecidível - o famoso problema da parada. É por isso que os cientistas da computação tiveram uma ideia brilhante, ou um grande hack, dependendo do seu ponto de vista, para estender cada tipo por mais um valor especial chamado bottom e denotado por `_|_`, ou Unicode ⊥. Este “valor” corresponde a um cálculo sem fim. Portanto, uma função declarada como:
```hs
f :: Bool -> Bool
```

pode retornar `True`, `False` ou `_|_`; o último significa que nunca terminaria.

Curiosamente, uma vez que você aceita o bottom como parte do sistema de tipos, é conveniente tratar todos os erros de tempo de execução como um bottom e até mesmo permitir que as funções retornem o bottom explicitamente. O último geralmente é feito usando a expressão `undefined`, como em:
```hs
f :: Bool -> Bool
f x = undefined
```

Este tipo de definição verifica [passa no teste de verificação] porque `undefined` avalia para `bottom`, que é um membro de qualquer tipo, incluindo `Bool`. Você pode até escrever:
```hs
f :: Bool -> Bool
f = undefined
```

(sem o `x`) porque bottom também é um membro do tipo `Bool -> Bool`.

As funções que podem retornar bottom são chamadas parciais, em oposição às funções totais, que retornam resultados válidos para todos os argumentos possíveis.

Por causa do bottom, você verá a categoria de tipos e funções de Haskell referidos como `Hask` em vez de `Set`. Do ponto de vista teórico, esta é a fonte de complicações sem fim, então, neste ponto, vou usar minha faca de açougueiro e encerrar esta linha de raciocínio. Do ponto de vista pragmático, não há problema em ignorar funções e bottom não-terminais e tratar `Hask` como um `Set` genuíno (consulte a Bibliografia no final).

#### Por que precisamos de um modelo matemático?
Como programador, você está intimamente familiarizado com a sintaxe e a gramática de sua linguagem de programação. Esses aspectos da linguagem são geralmente descritos usando notação formal no início da especificação da linguagem. Mas o significado, ou semântica, da linguagem é muito mais difícil de descrever; leva muito mais páginas, raramente é formal o suficiente e quase nunca é completo. Daí as discussões intermináveis ​​entre os advogados linguísticos e toda uma indústria artesanal de livros dedicada à exegese dos pontos mais delicados dos padrões linguísticos.

Existem ferramentas formais para descrever a semântica de uma linguagem, mas, por causa de sua complexidade, elas são usadas principalmente com linguagens acadêmicas simplificadas, não gigantes da programação da vida real. Uma dessas ferramentas, chamada de semântica operacional, descreve a mecânica de execução do programa. Ele define um intérprete idealizado formalizado. A semântica das linguagens industriais, como C++, geralmente é descrita usando raciocínio operacional informal, muitas vezes em termos de uma "máquina abstrata".

O problema é que é muito difícil provar coisas sobre programas usando a semântica operacional. Para mostrar uma propriedade de um programa, você essencialmente tem que “executá-la” por meio do interpretador idealizado.

Não importa que os programadores nunca realizem provas formais de correção. Sempre “pensamos” que escrevemos programas corretos. Ninguém se senta ao teclado dizendo: "Ah, vou apenas lançar algumas linhas de código e ver o que acontece." Achamos que o código que escrevemos realizará certas ações que produzirão os resultados desejados. Normalmente ficamos bastante surpresos quando isso não acontece. Isso significa que raciocinamos sobre os programas que escrevemos, e geralmente o fazemos executando um interpretador em nossas cabeças. É muito difícil controlar todas as variáveis. Os computadores são bons para executar programas - os humanos não! Se fôssemos, não precisaríamos de computadores.

Mas há uma alternativa. É chamada de semântica denotacional e é baseada na matemática. Na semântica denotacional, cada construção de programação recebe sua interpretação matemática. Com isso, se você quiser provar uma propriedade de um programa, basta provar um teorema matemático. Você pode pensar que provar teoremas é difícil, mas o fato é que nós, humanos, temos construído métodos matemáticos por milhares de anos, então há uma riqueza de conhecimento acumulado para explorar. Além disso, em comparação com o tipo de teorema que os matemáticos profissionais provam, os problemas que encontramos na programação são geralmente bastante simples, senão triviais.

Considere a definição de uma função fatorial em Haskell, que é uma linguagem bastante receptiva à semântica denotacional:
```hs
fact n = product [1..n]
```

A expressão `[1..n]` é uma lista de inteiros de 1 a n. A função `product` multiplica todos os elementos de uma lista. Isso é como uma definição de fatorial tirada de um texto matemático. Compare isso com C:
```c
int fact(int n) {
    int i;
    int result = 1;
    for (i = 2; i <= n; ++i)
        result *= i;
    return result;
}
```

Preciso dizer mais?

Ok, eu serei o primeiro a admitir que essa foi uma tentativa barata! Uma função fatorial tem uma denotação matemática óbvia. Um leitor astuto pode perguntar: Qual é o modelo matemático para ler um caractere do teclado ou enviar um pacote pela rede? Por muito tempo, essa teria sido uma pergunta incômoda, levando a uma explicação bastante complicada. Parecia que a semântica denotacional não era o melhor ajuste para um número considerável de tarefas importantes que eram essenciais para escrever programas úteis e que poderiam ser facilmente resolvidas pela semântica operacional. O avanço veio da teoria das categorias. Eugenio Moggi descobriu que o efeito computacional pode ser mapeado para mônadas. Isso acabou sendo uma observação importante que não apenas deu à semântica denotacional um novo sopro de vida e tornou os programas puramente funcionais mais utilizáveis, mas também lançou uma nova luz sobre a programação tradicional. Falarei sobre mônadas mais tarde, quando desenvolvermos ferramentas mais categóricas.

Uma das vantagens importantes de se ter um modelo matemático para programação é que é possível realizar provas formais de correção do software. Isso pode não parecer tão importante quando você está escrevendo um software de consumidor, mas existem áreas da programação onde o preço do fracasso pode ser exorbitante ou onde a vida humana está em jogo. Mas mesmo ao escrever aplicativos da web para o sistema de saúde, você pode apreciar a ideia de que funções e algoritmos da biblioteca padrão Haskell vêm com provas de correção.

#### Funções puras e sujas
As coisas que chamamos de funções em C++ ou em qualquer outra linguagem imperativa não são as mesmas coisas que os matemáticos chamam de funções. Uma função matemática é apenas um mapeamento de valores em valores.

Podemos implementar uma função matemática em uma linguagem de programação: Tal função, dado um valor de entrada, calculará o valor de saída. Uma função para produzir o quadrado de um número provavelmente multiplicará o valor de entrada por si mesma. Ele fará isso sempre que for chamado e com certeza produzirá a mesma saída sempre que for chamado com a mesma entrada. O quadrado de um número não muda com as fases da lua.

Além disso, calcular o quadrado de um número não deve ter o efeito colateral de dispensar uma guloseima saborosa para o seu cão. Uma “função” que faz isso não pode ser facilmente modelada como uma função matemática.

Em linguagens de programação, as funções que sempre produzem o mesmo resultado com a mesma entrada e não têm efeitos colaterais são chamadas de funções puras. Em uma linguagem puramente funcional como Haskell, todas as funções são puras. Por causa disso, é mais fácil dar a essas linguagens semântica denotacional e modelá-las usando a teoria das categorias. Quanto a outras linguagens, sempre é possível se restringir a um subconjunto puro ou raciocinar sobre os efeitos colaterais separadamente. Mais tarde, veremos como as mônadas nos permitem modelar todos os tipos de efeitos usando apenas funções puras. Então, nós realmente não perdemos nada ao nos restringirmos a funções matemáticas.

#### Exemplos de Tipos
Depois de perceber que os tipos são conjuntos, você pode pensar em alguns tipos bastante exóticos. Por exemplo, qual é o tipo correspondente a um conjunto vazio? Não, não é o `void` do C++, embora este tipo seja chamado `Void` em Haskell. É um tipo que não é habitado por nenhum valor. Você pode definir uma função que leva `Void`, mas você nunca pode chamá-la. Para chamá-lo, você teria que fornecer um valor do tipo `Void`, e simplesmente não há nenhum. Quanto ao que esta função pode devolver, não existem quaisquer restrições. Ele pode retornar qualquer tipo (embora nunca retorne, porque não pode ser chamado). Em outras palavras, é uma função polimórfica no tipo de retorno. Haskellers tem um nome para isso:
```hs
absurd :: Void -> a
```

(Lembre-se de que `a` é uma variável de tipo que pode representar qualquer tipo.) O nome não é coincidência. Há uma interpretação mais profunda de tipos e funções em termos de lógica chamada de isomorfismo de Curry-Howard. O tipo `Void` representa a falsidade, e o tipo da função `absurd` corresponde à afirmação de que da falsidade tudo segue, como no adágio latino “ex falso sequitur quodlibet”.

O próximo é o tipo que corresponde a um conjunto de singleton. É um tipo que possui apenas um valor possível. Este valor apenas “é”. Você pode não reconhecê-lo imediatamente como tal, mas esse é o `void` do C++. Pense nas funções deste tipo e para esse tipo. Uma função `void` sempre pode ser chamada. Se for uma função pura, sempre retornará o mesmo resultado. Aqui está um exemplo de tal função:
```cpp
int f44() {
    return 44;
}
```

Você pode pensar nesta função como "nada", mas como acabamos de ver, uma função que leva "nada" nunca pode ser chamada porque não há valor que representa "nada". Então, o que essa função leva? Conceitualmente, é necessário um valor fictício do qual existe apenas uma instância, então não temos que mencioná-lo explicitamente. Em Haskell, entretanto, há um símbolo para este valor: um par vazio de parênteses, `()`. Então, por uma coincidência engraçada (ou é uma coincidência?), A chamada para uma função de `void` parece a mesma em C++ e em Haskell. Além disso, por causa do amor de Haskell pela concisão, o mesmo símbolo `()` é usado para o tipo, o construtor e o único valor correspondente a um conjunto singleton. Então, aqui está esta função em Haskell:
```hs
f44 :: () -> Integer
f44 () = 44
```

A primeira linha declara que `f44` leva o tipo `()`, pronunciado “unidade”, para o tipo `Integer`. A segunda linha define `f44` pelo padrão que corresponde ao único construtor da unidade, `()`, e produz o número `44`. Você chama essa função fornecendo o valor da unidade `()`:
```hs
f44 ()
```

Observe que cada função de unidade é equivalente a escolher um único elemento do tipo de destino (aqui, escolher o inteiro `44`). Na verdade, você poderia pensar em `f44` como uma representação diferente para o número `44`. Este é um exemplo de como podemos substituir a menção explícita de elementos de um conjunto falando sobre funções (setas). As funções da unidade para qualquer tipo A estão em correspondência um a um com os elementos desse conjunto A.

E as funções com o tipo de retorno `void` ou, em Haskell, com o tipo de retorno de unidade? Em C++, essas funções são usadas para efeitos colaterais, mas sabemos que não são funções reais no sentido matemático da palavra. Uma função pura que retorna unidade não faz nada: ela descarta seu argumento.

Matematicamente, uma função de um conjunto A para um conjunto singleton mapeia cada elemento de A para o único elemento desse conjunto singleton. Para cada A, existe exatamente uma dessas funções. Esta é esta função para `Integer`:
```hs
fInt :: Integer -> ()
fInt x = ()
```

Você fornece a ele qualquer número inteiro e ele retorna uma unidade. No espírito de concisão, Haskell permite que você use o padrão curinga, o sublinhado, para um argumento que é descartado. Dessa forma, você não precisa inventar um nome para ele. Portanto, o acima pode ser reescrito como:
```hs
fInt :: Integer -> ()
fInt _ = ()
```

Observe que a implementação desta função não só não depende do valor passado a ela, mas nem mesmo depende do tipo do argumento.

Funções que podem ser implementadas com a mesma fórmula para qualquer tipo são chamadas parametricamente polimórficas. Você pode implementar uma família inteira de tais funções com uma equação usando um parâmetro de tipo em vez de um tipo concreto. O que devemos chamar de função polimórfica de qualquer tipo para tipo de unidade? Claro, vamos chamá-lo de `unit`:
```hs
unit :: a -> ()
unit _ = ()
```

Em C++, você escreveria esta função como:
```cpp
template<class T>
void unit(T) {}
```

O próximo na tipologia de tipos é um conjunto de dois elementos. Em C++ é chamado de `bool` e em Haskell, previsivelmente, `Bool`. A diferença é que em C++ `bool` é um tipo integrado, enquanto em Haskell pode ser definido da seguinte maneira:
```hs
data Bool = True | False
```

(A maneira de ler esta definição é que `Bool` é `True` ou `False`.) Em princípio, também se deve ser capaz de definir um tipo `Bool` em C++ como uma enumeração:
```cpp
enum bool {
     true,
     false
};
```

mas o `enum` do C++ é secretamente um número inteiro. A “classe enum” do C++ 11 poderia ter sido usada em vez disso, mas então você teria que qualificar seus valores com o nome da classe, como em `bool::true` e `bool::false`, sem mencionar a necessidade de incluir o cabeçalho apropriado em cada arquivo que o usa.

As funções puras de `Bool` apenas escolhem dois valores do tipo de destino, um correspondente a `True` e outro a `False`.

Funções para `Bool` são chamadas de predicados. Por exemplo, a biblioteca Haskell `Data.Char` está cheia de predicados como `isAlpha` ou `isDigit`. Em C++ existe uma biblioteca semelhante que define, entre outras, `isalpha` e `isdigit`, mas estes retornam um `int` em vez de um `bool`. Os predicados reais são definidos em `std::ctype` e têm a forma `ctype::is(alpha, c)`, `ctype::is(digit, c)`, etc.

#### Desafios
1. Defina uma função de ordem superior (ou um objeto de função) `memoize` em sua linguagem preferida. Esta função recebe uma função pura `f` como um argumento e retorna uma função que se comporta quase exatamente como `f`, exceto que ela apenas chama a função original uma vez para cada argumento, armazena o resultado internamente e, subsequentemente, retorna este resultado armazenado toda vez que é chamado com o mesmo argumento. Você pode diferenciar a função memorizada do original observando seu desempenho. Por exemplo, tente memorizar uma função que leva muito tempo para ser avaliada. Você terá que esperar pelo resultado na primeira vez que chamá-lo, mas nas chamadas subsequentes, com o mesmo argumento, você deve obter o resultado imediatamente.
2. Tente memorizar uma função de sua biblioteca padrão que você normalmente usa para produzir números aleatórios. Funciona?
3. A maioria dos geradores de números aleatórios pode ser inicializada com uma semente. Implemente uma função que obtém uma semente, chama o gerador de número aleatório com aquela semente e retorna o resultado. Memoize essa função. Funciona?
4. Quais dessas funções C++ são puras? Tente memorizá-los e observe o que acontece quando você liga para eles várias vezes: memorizados ou não.
    1. A função fatorial do exemplo no texto.
    2. `std::getchar()`
    3. `bool f() { std::cout << "Hello!" << std::endl; return true; }`
    4. `int f(int x) { static int y = 0; y += x; return y; }`
5. Quantas funções diferentes existem de `Bool` para `Bool`? Você pode implementar todos eles?
6. Faça um desenho de uma categoria cujos únicos objetos sejam os tipos `Void`, `()` (unidade) e `Bool`; com setas correspondendo a todas as funções possíveis entre esses tipos. Identifique as setas com os nomes das funções.

#### Bibliografia
Nils Anders Danielsson, John Hughes, Patrik Jansson, Jeremy Gibbons, [Fast and Loose Reasoning is Morally Correct](http://www.cs.ox.ac.uk/jeremy.gibbons/publications/fast+loose.pdf). Este artigo fornece justificativa para ignorar bottom na maioria dos contextos.

### Categorias grandes e pequenas
Você pode obter uma apreciação real das categorias estudando uma variedade de exemplos. As categorias vêm em todas as formas e tamanhos e costumam aparecer em lugares inesperados. Vamos começar com algo realmente simples.

#### Sem objetos
A categoria mais trivial é aquela com zero objetos e, consequentemente, zero morfismos. É uma categoria muito triste por si só, mas pode ser importante no contexto de outras categorias, por exemplo, na categoria de todas as categorias (sim, existe uma). Se você acha que um conjunto vazio faz sentido, por que não uma categoria vazia?

#### Grafos simples
Você pode construir categorias apenas conectando objetos com setas. Você pode imaginar começar com qualquer grafo direcionado e transformá-lo em uma categoria simplesmente adicionando mais setas. Primeiro, adicione uma seta de identidade em cada nó. Então, para quaisquer duas setas, de modo que o final de uma coincida com o início da outra (em outras palavras, quaisquer duas setas composíveis), adicione uma nova seta para servir como sua composição. Cada vez que você adiciona uma nova seta, você também deve considerar sua composição com qualquer outra seta (exceto para as setas de identidade) e com ela mesma. Você geralmente acaba com um número infinito de setas, mas tudo bem.

Outra maneira de ver esse processo é que você está criando uma categoria, que tem um objeto para cada nó no grafo e todas as cadeias possíveis de arestas de grafos como morfismos. (Você pode até considerar morfismos de identidade como casos especiais de cadeias de comprimento zero.)

Essa categoria é chamada de categoria livre gerada por um determinado grafo. É um exemplo de construção livre, um processo de completar uma determinada estrutura, estendendo-a com um número mínimo de itens para satisfazer suas leis (aqui, as leis de uma categoria). Veremos mais exemplos disso no futuro.

#### Ordens
E agora para algo completamente diferente! Uma categoria em que um morfismo é uma relação entre objetos: a relação de ser menor ou igual. Vamos verificar se realmente é uma categoria. Temos morfismos de identidade? Cada objeto é menor ou igual a si mesmo: verifique! Temos composição? Se a <= b e b <= c, então a <= c: verifique! A composição é associativa? Verifique! Um conjunto com uma relação como essa é chamado de pré-ordem, portanto, uma pré-ordem é de fato uma categoria.

Você também pode ter uma relação mais forte, que satisfaça uma condição adicional de que, se a <= b e b <= a, então a deve ser igual a b. Isso é chamado de ordem parcial.

Finalmente, você pode impor a condição de que quaisquer dois objetos estejam em relação um com o outro, de uma forma ou de outra; e isso dá a você uma ordem linear ou ordem total.

Vamos caracterizar esses conjuntos ordenados como categorias. Uma pré-ordem é uma categoria em que há no máximo um morfismo indo de qualquer objeto `a` para qualquer objeto `b`. Outro nome para essa categoria é “magro”. Uma pré-ordem é uma categoria pequena.

Um conjunto de morfismos do objeto `a` ao objeto `b` em uma categoria `C` é chamado de conjunto homo e é escrito como `C(a, b)` (ou, às vezes, $`Hom_C(a, b)`$). Portanto, cada conjunto hom em uma pré-ordem está vazio ou é um singleton. Isso inclui o conjunto hom `C(a, a)`, o conjunto de morfismos de `a` a `a`, que deve ser um singleton, contendo apenas a identidade, em qualquer pré-ordem. Você pode, no entanto, ter ciclos em uma pré-ordem. Os ciclos são proibidos em uma ordem parcial.

É muito importante ser capaz de reconhecer pré-ordens, ordens parciais e ordens totais por causa da classificação. Os algoritmos de classificação, como quicksort, bubble sort, merge sort, etc, só podem funcionar corretamente em ordens totais. Ordens parciais podem ser classificadas usando classificação topológica.

#### Monóide como conjunto
Monoid é um conceito embaraçosamente simples, mas incrivelmente poderoso. É o conceito por trás da aritmética básica: tanto a adição quanto a multiplicação formam um monóide. Monóides são onipresentes na programação. Eles aparecem como strings, listas, estruturas de dados dobráveis, futuros em programação simultânea, eventos em programação reativa funcional e assim por diante.

Tradicionalmente, um monóide é definido como um conjunto com uma operação binária. Tudo o que é necessário para esta operação é que seja associativa e que haja um elemento especial que se comporte como uma unidade em relação a ele.

Por exemplo, números naturais com zero formam um monóide sob adição. Associatividade significa que:
`(a + b) + c = a + (b + c)`

(Em outras palavras, podemos pular os parênteses ao adicionar números.)

O elemento neutro é zero, porque:
`0 + a = a`

e
`a + 0 = a`

A segunda equação é redundante, porque a adição é comutativa (`a + b = b + a`), mas a comutatividade não faz parte da definição de um monóide. Por exemplo, a concatenação de strings não é comutativa e ainda assim forma um monóide. O elemento neutro para concatenação de string, a propósito, é uma string vazia, que pode ser anexada a qualquer lado de uma string sem alterá-la.

Em Haskell, podemos definir uma classe de tipo para monoides - um tipo para o qual existe um elemento neutro chamado `mempty` e uma operação binária chamada `mappend`:
```hs
class Monoid m where
    mempty  :: m
    mappend :: m -> m -> m
```

A assinatura de tipo para uma função de dois argumentos, `m -> m -> m`, pode parecer estranha a princípio, mas fará todo o sentido depois que falarmos sobre currying. Você pode interpretar uma assinatura com várias setas de duas maneiras básicas: como uma função de vários argumentos, com o tipo mais à direita sendo o tipo de retorno; ou como uma função de um argumento (o mais à esquerda), retornando uma função. A última interpretação pode ser enfatizada adicionando parênteses (que são redundantes, porque a seta é associativa à direita), como em: `m -> (m -> m)`. Voltaremos a esta interpretação em um momento.

Observe que, em Haskell, não há maneira de expressar as propriedades monoidais de `mempty` e `mappend` (ou seja, o fato de que `mempty` é neutro e `mappend` é associativo). É responsabilidade do programador certificar-se de que estão satisfeitos.

As classes Haskell não são tão intrusivas quanto as classes C++. Ao definir um novo tipo, você não precisa especificar sua classe antecipadamente. Você está livre para procrastinar e declarar um determinado tipo como uma instância de alguma classe muito mais tarde. Como exemplo, vamos declarar `String` como um monóide, fornecendo a implementação de `mempty` e `mappend` (isso é, de fato, feito para você na `Prelude` padrão):
```hs
instance Monoid String where
    mempty = ""
    mappend = (++)
```

Aqui, reutilizamos o operador de concatenação de lista `(++)`, porque uma `String` é apenas uma lista de caracteres.

Uma palavra sobre a sintaxe Haskell: qualquer operador infixo pode ser transformado em uma função de dois argumentos envolvendo-o entre parênteses. Dadas duas strings, você pode concatená-las inserindo `++` entre elas:
```hs
"Olá " ++ "mundo!"
```

ou passando-os como dois argumentos entre parênteses `(++)`:
```hs
(++) "Olá " "mundo!"
```

Observe que os argumentos para uma função não são separados por vírgulas ou entre parênteses. (Esta é provavelmente a coisa mais difícil de se acostumar ao aprender Haskell.)

É importante enfatizar que Haskell permite expressar igualdade de funções, como em:
```hs
mappend = (++)
```

Conceitualmente, isso é diferente de expressar a igualdade de valores produzidos por funções, como em:
```hs
mappend s1 s2 = (++) s1 s2
```

O primeiro se traduz em igualdade de morfismos na categoria `Hask` (ou `Set`, se ignorarmos bottoms, que é o nome dos cálculos intermináveis). Essas equações não são apenas mais sucintas, mas muitas vezes podem ser generalizadas para outras categorias. A última é chamada de igualdade extensional e afirma o fato de que para quaisquer duas strings de entrada, as saídas de `mappend` e `(++)` são as mesmas. Como os valores dos argumentos às vezes são chamados de pontos (como em: o valor de `f` no ponto `x`), isso é chamado de igualdade ponto-a-ponto. A igualdade da função sem especificar os argumentos é descrita como sem pontos. (A propósito, as equações livres de pontos geralmente envolvem a composição de funções, que é simbolizada por um ponto, então isso pode ser um pouco confuso para o iniciante.)

O mais próximo que se pode chegar de declarar um monóide em C++ seria usar a sintaxe (proposta) para concepts.
```cpp
template<class T>
  T mempty = delete;

template<class T>
  T mappend(T, T) = delete;

template<class M>
  concept bool Monoid = requires (M m) {
    { mempty<M> } -> M;
    { mappend(m, m); } -> M;
  };
```

A primeira definição usa um modelo de valor (também proposto). Um valor polimórfico é uma família de valores - um valor diferente para cada tipo.

A palavra-chave `delete` significa que não há um valor padrão definido: ele terá que ser especificado caso a caso. Da mesma forma, não há padrão para `mappend`.

O concept `Monoid` é um predicado (daí o tipo `bool`) que testa se existem definições apropriadas de `mempty` e `mappend` para um determinado tipo `M`.

Uma instanciação do concept `Monoid` pode ser realizada fornecendo specializations e overloads apropriados:
```cpp
template<>
std::string mempty<std::string> = {""};

std::string mappend(std::string s1, std::string s2) {
    return s1 + s2;
}
```

#### Monóide como categoria
Essa era a definição “familiar” do monóide em termos de elementos de um conjunto. Mas, como você sabe, na teoria das categorias tentamos nos afastar dos conjuntos e de seus elementos e, em vez disso, falar sobre objetos e morfismos. Então, vamos mudar nossa perspectiva um pouco e pensar na aplicação do operador binário como "mover" ou "mudar" as coisas ao redor do conjunto.

Por exemplo, existe a operação de adicionar 5 a cada número natural. Ele mapeia de 0 a 5, 1 a 6, 2 a 7 e assim por diante. Essa é uma função definida no conjunto de números naturais. Isso é bom: temos uma função e um conjunto. Em geral, para qualquer número n, há uma função de adicionar n (o “somador” de n).

Como os somadores compõem? A composição da função que adiciona 5 com a função que adiciona 7 é uma função que adiciona 12. Portanto, a composição dos somadores pode ser tornada equivalente às regras de adição. Isso também é bom: podemos substituir adição por composição de função.

Mas espere, há mais: há também o somador para o elemento neutro, zero. Adicionar zero não move as coisas, então é a função de identidade no conjunto de números naturais.

Em vez de fornecer as regras tradicionais de adição, também poderia fornecer as regras de composição de somadores, sem nenhuma perda de informação. Observe que a composição dos somadores é associativa, porque a composição das funções é associativa; e temos o somador zero correspondente à função de identidade.

Um leitor astuto pode ter notado que o mapeamento de inteiros para somadores segue da segunda interpretação da assinatura de tipo de mappend como `m -> (m -> m)`. Ele nos diz que `mappend` mapeia um elemento de um conjunto monóide para uma função que atua naquele conjunto.

Agora, quero que você esqueça que está lidando com o conjunto de números naturais e apenas pense nele como um único objeto, uma bolha com um monte de morfismos - os somadores. Um monóide é uma única categoria de objeto. Na verdade, o nome monóide vem do grego mono, que significa único. Cada monóide pode ser descrito como uma única categoria de objeto com um conjunto de morfismos que seguem regras de composição apropriadas.

![](images/image_3.jpg)

A concatenação de string é um caso interessante, porque temos a opção de definir apêndices direitos e apêndices esquerdos (ou antepassados, se preferir). As tabelas de composição dos dois modelos são um espelho reverso uma da outra. Você pode facilmente se convencer de que acrescentar “bar” após “foo” corresponde a acrescentar “foo” após acrescentar “bar”.

Você pode perguntar se cada monóide categórico - uma categoria de um objeto - define um único monóide conjunto com operador binário. Acontece que sempre podemos extrair um conjunto de uma categoria de objeto único. Este conjunto é o conjunto de morfismos - os somadores em nosso exemplo. Em outras palavras, temos o conjunto hom `M(m, m)` do único objeto `m` na categoria `M`. Podemos facilmente definir um operador binário neste conjunto: O produto monoidal de dois conjuntos de elementos é o elemento correspondente a composição dos morfismos correspondentes. Se você me der dois elementos de `M(m, m)` correspondentes a `f` e `g`, seu produto corresponderá à composição `g∘f`. A composição sempre existe, porque a origem e o destino desses morfismos são o mesmo objeto. E é associativo pelas regras da categoria. O morfismo de identidade é o elemento neutro deste produto. Portanto, sempre podemos recuperar um monóide definido de um monóide de categoria. Para todos os efeitos e propósitos, eles são um e o mesmo.

![](images/image_4.jpg)

<em>Conjunto hom monoidal visto como morfismos e como pontos em um conjunto</em>

Há apenas um pequeno detalhe para os matemáticos escolherem: os morfismos não precisam formar um conjunto. No mundo das categorias, existem coisas maiores do que conjuntos. Uma categoria na qual morfismos entre quaisquer dois objetos formam um conjunto é chamada de localmente pequeno. Como prometido, estarei ignorando principalmente essas sutilezas, mas achei que deveria mencioná-las para registro.

Muitos fenômenos interessantes na teoria das categorias têm sua raiz no fato de que os elementos de um conjunto hom podem ser vistos tanto como morfismos, que seguem as regras de composição, quanto como pontos em um conjunto. Aqui, a composição de morfismos em `M` se traduz em produto monoidal no conjunto `M(m, m)`.

#### Agradecimentos
Gostaria de agradecer a Andrew Sutton por reescrever meu código de conceito de monóide em C++ de acordo com a proposta mais recente dele e de Bjarne Stroustrup.

#### Desafios
1. Gere uma categoria livre de:
    1. Um grafo com um nó e sem arestas
    2. Um grafo com um nó e uma aresta (direcionada) (dica: esta aresta pode ser composta por ela mesma)
    3. Um grafo com dois nós e uma única seta entre eles
    4. Um grafo com um único nó e 26 setas marcadas com as letras do alfabeto: a, b, c... z.
2. Que tipo de ordem é essa?
    1. Um conjunto de conjuntos com a relação de inclusão: A está incluído em B se cada elemento de A também for um elemento de B.
    2. Tipos C++ com a seguinte relação de subtipos: T1 é um subtipo de T2 se um ponteiro para T1 pode ser passado para uma função que espera um ponteiro para T2 sem disparar um erro de compilação.
3. Considerando que `Bool` é um conjunto de dois valores `True` e `False`, mostre que ele forma dois monóides (teóricos do conjunto) em relação ao, respectivamente, operador && (AND) e || (OR).
4. Represente o monóide `Bool` com o operador AND como uma categoria: Liste os morfismos e suas regras de composição.
5. Represente a adição módulo 3 como uma categoria monoidal.

### Categorias Kleisli

#### Composição de logs
Você viu como modelar tipos e funções puras como uma categoria. Também mencionei que existe uma maneira de modelar efeitos colaterais, ou funções não puras, na teoria das categorias. Vamos dar uma olhada em um exemplo: funções que registram ou rastreiam sua execução. Algo que, em uma linguagem imperativa, provavelmente seria implementado pela mutação de algum estado global, como em:
```cpp
string logger;

bool negate(bool b) {
     logger += "Não é assim! ";
     return !b;
}
```

Você sabe que esta não é uma função pura, porque sua versão memoized não produziria um log. Esta função tem efeitos colaterais.

Na programação moderna, tentamos ficar longe do estado mutável global tanto quanto possível - apenas por causa das complicações da simultaneidade. E você nunca colocaria um código como este em uma biblioteca.

Felizmente para nós, é possível tornar essa função pura. Você apenas tem que passar o log explicitamente, dentro e fora. Vamos fazer isso adicionando um argumento de string e pareando a saída regular com uma string que contém o log atualizado:
```cpp
pair<bool, string> negate(bool b, string logger) {
     return make_pair(!b, logger + "Não é assim! ");
}
```

Esta função é pura, não tem efeitos colaterais, retorna o mesmo par sempre que é chamada com os mesmos argumentos e pode ser memorizada, se necessário. No entanto, considerando a natureza cumulativa do log, você teria que memorizar todos os históricos possíveis que podem levar a uma determinada chamada. Haveria uma entrada de memorando separada para:

```cpp
negate(true, "Foi o melhor dos tempos. ");
```

e

```cpp
negate(true, "Foi o pior dos tempos. ");
```

e assim por diante.

Também não é uma interface muito boa para uma função de biblioteca. Os chamadores são livres para ignorar a string no tipo de retorno, então isso não é um grande fardo; mas eles são forçados a passar uma string como entrada, o que pode ser inconveniente.

Existe uma maneira de fazer a mesma coisa de forma menos intrusiva? Existe uma maneira de separar as preocupações? Neste exemplo simples, o objetivo principal da função `negate` é transformar um booleano em outro. O registro é secundário. Concedido, a mensagem registrada é específica para a função, mas a tarefa de agregar as mensagens em um registro contínuo é uma preocupação separada. Ainda queremos que a função produza uma string, mas gostaríamos de aliviá-la da produção de um log. Então, aqui está a solução de compromisso:
```cpp
pair<bool, string> negate(bool b) {
      return make_pair(!b, "Não é assim! ");
}
```

A ideia é que o log seja agregado entre as chamadas de função.

Para ver como isso pode ser feito, vamos mudar para um exemplo um pouco mais realista. Temos uma função de string em string que transforma caracteres de minúsculas em maiúsculas:

```cpp
string toUpper(string s) {
    string result;
    int (*toupperp)(int) = &toupper; // toupper está overloaded
    transform(begin(s), end(s), back_inserter(result), toupperp);
    return result;
}
```

e outro que divide uma string em um vetor de strings, quebrando-o nos limites dos espaços em branco:
```cpp
vector<string> toWords(string s) {
    return words(s);
}
```

O trabalho real é feito na função auxiliar `words`:
```cpp
vector<string> words(string s) {
    vector<string> result{""};
    for (auto i = begin(s); i != end(s); ++i)
    {
        if (isspace(*i))
            result.push_back("");
        else
            result.back() += *i;
    }
    return result;
}
```

Queremos modificar as funções `toUpper` e `toWords` para que adicionem uma string de mensagem aos seus valores de retorno regulares.

Vamos “embelezar” os valores de retorno dessas funções. Vamos fazer isso de maneira genérica, definindo um template `Writer` que encapsula um par cujo primeiro componente é um valor do tipo arbitrário A e o segundo componente é uma string:
```cpp
template<class A>
using Writer = pair<A, string>;
```

Aqui estão as funções embelezadas:
```cpp
Writer<string> toUpper(string s) {
    string resultado;
    int (*toupperp)(int) = &toupper;
    transform(begin(s), end(s), back_inserter(result), toupperp);
    return make_pair(result, "toUpper ");
}

Writer<vector<string>> toWords(string s) {
    return make_pair(words(s), "toWords ");
}
```

Queremos compor essas duas funções em outra função embelezada que coloca uma string em maiúsculas e a divide em palavras, ao mesmo tempo em que produz um registro dessas ações. Veja como podemos fazer isso:
```cpp
Writer<vector<string>> process(string s) {
    auto p1 = toUpper(s);
    auto p2 = toWords(p1.first);
    return make_pair(p2.first, p1.second + p2.second);
}
```

Cumprimos nosso objetivo: a agregação do log não é mais uma preocupação das funções individuais. Eles produzem suas próprias mensagens, que são então, externamente, concatenadas em um log maior.

Agora imagine um programa completo escrito neste estilo. É um pesadelo de código repetitivo e sujeito a erros. Mas somos programadores. Sabemos como lidar com código repetitivo: nós o abstraimos! Esta não é, no entanto, sua abstração comum - temos que abstrair a própria composição de funções. Mas a composição é a essência da teoria das categorias, então antes de escrevermos mais código, vamos analisar o problema do ponto de vista categórico.

#### A categoria Writer
A ideia de embelezar os tipos de retorno de um grupo de funções para pegar algumas funcionalidades adicionais acabou sendo muito frutífera. Veremos muitos outros exemplos disso. O ponto de partida é nossa categoria regular de tipos e funções. Vamos deixar os tipos como objetos, mas redefinir nossos morfismos para serem as funções embelezadas.

Por exemplo, suponha que queremos embelezar a função `isEven` que vai de `int` para `bool`. Nós o transformamos em um morfismo que é representado por uma função embelezada. O importante é que esse morfismo ainda é considerado uma seta entre os objetos `int` e `bool`, embora a função embelezada retorne um par:
```cpp
pair<bool, string> isEven(int n) {
     return make_pair(n % 2 == 0, "isEven ");
}
```

Pelas leis de uma categoria, deveríamos ser capazes de compor esse morfismo com outro morfismo que vai do objeto `bool` a qualquer coisa. Em particular, devemos ser capazes de compor com nosso `negate` anterior:
```cpp
pair<bool, string> negate(bool b) {
     return make_pair(!b, "Não é assim! ");
}
```

Obviamente, não podemos compor esses dois morfismos da mesma forma que compomos funções regulares, devido à incompatibilidade de entrada/saída. Sua composição deve ser mais parecida com esta:
```cpp
pair<bool, string> isOdd(int n) {
    pair<bool, string> p1 = isEven(n);
    pair<bool, string> p2 = negate(p1.first);
    return make_pair(p2.first, p1.second + p2.second);
}
```

Então, aqui está a receita para a composição de dois morfismos nesta nova categoria que estamos construindo:
1. Execute a função embelezada correspondente ao primeiro morfismo
2. Extraia o primeiro componente do par de resultados e passe-o para a função embelezada correspondente ao segundo morfismo
3. Concatene o segundo componente (a string) do primeiro resultado e o segundo componente (a string) do segundo resultado
4. Retorne um novo par combinando o primeiro componente do resultado final com a string concatenada.

Se quisermos abstrair essa composição como uma função de ordem superior em C++, temos que usar um modelo parametrizado por três tipos correspondentes a três objetos em nossa categoria. Deve ter duas funções embelezadas que são combináveis de acordo com nossas regras e retornar uma terceira função embelezada:
```cpp
template<class A, class B, class C>
function<Writer<C>(A)> compose(function<Writer<B>(A)> m1, 
                               function<Writer<C>(B)> m2)
{
    return [m1, m2](A x) {
        auto p1 = m1(x);
        auto p2 = m2(p1.first);
        return make_pair(p2.first, p1.second + p2.second);
    };
}
```

Agora podemos voltar ao nosso exemplo anterior e implementar a composição de `toUpper` e `toWords` usando este novo modelo:
```cpp
Writer<vector<string>> process(string s) {
   return compose<string, string, vector<string>>(toUpper, toWords)(s);
}
```

Ainda há muito ruído com a passagem de tipos para o modelo de composição. Isso pode ser evitado contanto que você tenha um compilador compatível com C++ 14 que suporte funções lambda generalizadas com dedução de tipo de retorno (o crédito por este código vai para Eric Niebler):
```cpp
auto const compose = [](auto m1, auto m2) {
    return [m1, m2](auto x) {
        auto p1 = m1(x);
        auto p2 = m2(p1.first);
        return make_pair(p2.first, p1.second + p2.second);
    };
};
```

Nesta nova definição, a implementação do processo simplifica para:
```cpp
Writer<vector<string>> process(string s){
   return compose(toUpper, toWords)(s);
}
```

Mas ainda não terminamos. Definimos composição em nossa nova categoria, mas quais são os morfismos de identidade? Essas não são nossas funções regulares de identidade! Eles têm que ser morfismos do tipo A para o tipo A, o que significa que são funções embelezadas da forma:
```cpp
Writer<A> identity(A);
```

Eles têm que se comportar como unidades no que diz respeito à composição. Se você olhar para nossa definição de composição, verá que um morfismo de identidade deve passar seu argumento sem alteração e apenas contribuir com uma string vazia para o log:
```cpp
template<class A>
Writer<A> identity(A x) {
    return make_pair(x, "");
}
```

Você pode facilmente se convencer de que a categoria que acabamos de definir é de fato uma categoria legítima. Em particular, nossa composição é trivialmente associativa. Se você seguir o que está acontecendo com o primeiro componente de cada par, é apenas uma composição de função regular, que é associativa. Os segundos componentes estão sendo concatenados e a concatenação também é associativa.

Um leitor astuto pode notar que seria fácil generalizar essa construção para qualquer monóide, não apenas para o monóide de strings. Usaríamos `mappend` dentro de `compose` e `mempty` dentro de `identity` (no lugar de `+` e `""`). Realmente não há razão para nos limitarmos a registrar apenas strings. Um bom escritor de biblioteca deve ser capaz de identificar o mínimo de restrições que fazem a biblioteca funcionar - aqui, o único requisito da biblioteca de registro é que o registro tenha propriedades monoidais.

#### Writer em Haskell
A mesma coisa em Haskell é um pouco mais concisa e também recebemos muito mais ajuda do compilador. Vamos começar definindo o tipo de `Writer`:
```hs
type Writer a = (a, String)
```

Aqui, estou apenas definindo um type alias [sinônimo de tipos], um equivalente a um `typedef` (ou `using`) em C++. O tipo `Writer` é parametrizado por uma variável de tipo `a` e é equivalente a um par de `a` e `String`. A sintaxe para pares é mínima: apenas dois itens entre parênteses, separados por vírgula.

Nossos morfismos são funções de um tipo arbitrário para algum tipo de `Writer`:
```hs
a -> Writer b
```

Declararemos a composição como um operador infixo engraçado, às vezes chamado de "fish":
```hs
(>=>) :: (a -> Writer b) -> (b -> Writer c) -> (a -> Writer c)
```

É uma função de dois argumentos, cada um sendo uma função própria e retornando uma função. O primeiro argumento é do tipo `(a -> Writer b)`, o segundo é `(b -> Writer c)`, e o resultado é `(a -> Writer c)`.

Aqui está a definição deste operador infixo - os dois argumentos `m1` e `m2` aparecendo em ambos os lados do símbolo de peixe:
```hs
m1 >=> m2 = \x -> 
    let (y, s1) = m1 x
        (z, s2) = m2 y
    in (z, s1 ++ s2)
```

O resultado é uma função lambda de um argumento `x`. O lambda é escrito como uma barra invertida - pense nisso como a letra grega λ com uma perna amputada.

A expressão `let` permite declarar variáveis auxiliares. Aqui, o resultado da chamada para `m1` é o padrão correspondido a um par de variáveis `(y, s1)`; e o resultado da chamada a `m2`, com o argumento `y` do primeiro padrão, é correspondido a `(z, s2)`.

É comum em Haskell padronizar pares de correspondência, em vez de usar acessores, como fizemos em C++. Fora isso, há uma correspondência bastante direta entre as duas implementações.

O valor geral da expressão `let` é especificado em sua cláusula `in`: aqui está um par cujo primeiro componente é `z` e o segundo componente é a concatenação de duas strings, `s1 ++ s2`.

Também definirei o morfismo de identidade para nossa categoria, mas por razões que ficarão claras muito mais tarde, chamarei de `return`.
```hs
return :: a -> Writer a
return x = (x, "")
```

Para completar, vamos ter as versões Haskell das funções embelezadas `upperCase` e `toWords`:
```hs
upCase :: String -> Writer String
upCase s = (map toUpper s, "upCase ")

toWords :: String -> Writer [String]
toWords s = (words s, "toWords ")
```

A função `map` corresponde ao `transform` de C++. Ele aplica a função de caractere `toUpper` à string `s`. A função auxiliar `words` está definida na biblioteca Prelude padrão.

Finalmente, a composição das duas funções é realizada com a ajuda do operador fish:
```hs
process :: String -> Writer [String]
process = upCase >=> toWords
```

#### Categorias Kleisli
Você deve ter adivinhado que não inventei essa categoria na hora. É um exemplo da chamada categoria Kleisli - uma categoria baseada em uma mônada. Ainda não estamos prontos para discutir as mônadas, mas gostaria de dar uma amostra do que elas podem fazer. Para nossos objetivos limitados, uma categoria Kleisli tem, como objetos, os tipos da linguagem de programação subjacente. Morfismos do tipo A para o tipo B são funções que vão de A para um tipo derivado de B usando o embelezamento particular. Cada categoria de Kleisli define sua própria maneira de compor tais morfismos, bem como os morfismos de identidade com relação a essa composição. (Mais tarde veremos que o termo impreciso "embelezamento" corresponde à noção de um endofuntor em uma categoria.)

A mônada específica que usei como base da categoria neste post é chamada de mônada writer e é usada para registrar ou rastrear a execução de funções. É também um exemplo de um mecanismo mais geral para incorporar efeitos em cálculos puros. Você viu anteriormente que poderíamos modelar tipos de linguagem de programação e funções na categoria de conjuntos (desconsiderando bottoms, como de costume). Aqui, estendemos este modelo a uma categoria ligeiramente diferente, uma categoria em que morfismos são representados por funções embelezadas, e sua composição faz mais do que apenas passar a saída de uma função para a entrada de outra. Temos mais um grau de liberdade para brincar: a própria composição. Acontece que esse é exatamente o grau de liberdade que torna possível dar semântica denotacional simples a programas que em linguagens imperativas são tradicionalmente implementados usando efeitos colaterais.

#### Desafio
Uma função que não é definida para todos os valores possíveis de seu argumento é chamada de função parcial. Não é realmente uma função no sentido matemático, então não se encaixa no molde categórico padrão. Ele pode, no entanto, ser representado por uma função que retorna um tipo opcional embelezado:
```cpp
template<class A> class optional {
    bool _isValid;
    A    _value;
public:
    optional()    : _isValid(false) {}
    optional(A v) : _isValid(true), _value(v) {}
    bool isValid() const { return _isValid; }
    A value() const { return _value; }
};
```

Como exemplo, aqui está a implementação da função embelezada `safe_root`:
```cpp
optional<double> safe_root(double x) {
    if (x >= 0) return optional<double>{sqrt(x)};
    else return optional<double>{};
}
```

Aqui está o desafio:
1. Construa a categoria Kleisli para funções parciais (defina composição e identidade).
2. Implemente a função embelezada `safe_reciprocal` que retorna um recíproco válido de seu argumento, se for diferente de zero.
3. Componha `safe_root` e `safe_reciprocal` para implementar `safe_root_reciprocal` que calcula `sqrt(1 / x)` sempre que possível.

#### Agradecimentos
Sou grato a Eric Niebler por ler o rascunho e fornecer a implementação inteligente do `compose` que usa recursos avançados do C++ 14 para conduzir a inferência de tipo. Eu fui capaz de cortar toda a seção de templates mágicos antigos que faziam a mesma coisa usando type traits. Boa viagem! Também sou grato a Gershom Bazerman pelos comentários úteis que me ajudaram a esclarecer alguns pontos importantes.

### Produtos e coprodutos
#### Siga as setas
O dramaturgo grego antigo Eurípides disse uma vez: "Todo homem é como a companhia que costuma ter." Somos definidos por nossos relacionamentos. Em nenhum lugar isso é mais verdadeiro do que na teoria das categorias. Se quisermos destacar um objeto específico em uma categoria, só podemos fazer isso descrevendo seu padrão de relacionamento com outros objetos (e com ele mesmo). Essas relações são definidas por morfismos.

Há uma construção comum na teoria das categorias chamada construção universal para definir objetos em termos de seus relacionamentos. Uma maneira de fazer isso é escolher um padrão, uma forma particular construída a partir de objetos e morfismos, e procurar todas as suas ocorrências na categoria. Se for um padrão bastante comum e a categoria for grande, é provável que você tenha muitos e muitos acertos. O truque é estabelecer algum tipo de classificação entre esses acertos e escolher o que pode ser considerado o mais adequado.

Este processo é uma reminiscência da forma como fazemos pesquisas na web. Uma consulta é como um padrão. Uma consulta muito geral fornecerá uma grande lembrança: muitos acessos. Alguns podem ser relevantes, outros não. Para eliminar acessos irrelevantes, você refina sua consulta. Isso aumenta sua precisão. Por fim, o mecanismo de pesquisa classificará os resultados e, com sorte, o resultado em que você está interessado estará no topo da lista.

#### Objeto inicial
A forma mais simples é um único objeto. Obviamente, existem tantas instâncias dessa forma quanto objetos em uma determinada categoria. Isso é muito por onde escolher. Precisamos estabelecer algum tipo de classificação e tentar encontrar o objeto que está no topo dessa hierarquia. Os únicos meios à nossa disposição são os morfismos. Se você pensar em morfismos como setas, então é possível que haja um fluxo geral líquido de setas de uma extremidade da categoria para outra. Isso é verdade em categorias ordenadas, por exemplo, em ordens parciais. Poderíamos generalizar essa noção de precedência de objeto dizendo que o objeto `a` é “mais inicial” do que o objeto `b` se houver uma seta (a morfismo) indo de `a` para `b`. Em seguida, definiríamos o objeto inicial como aquele que possui setas indo para todos os outros objetos. Obviamente, não há garantia de que tal objeto exista, e está tudo bem. Um problema maior é que pode haver muitos desses objetos: o recall é bom, mas falta precisão. A solução é dar uma dica nas categorias ordenadas - elas permitem no máximo uma seta entre quaisquer dois objetos: só existe uma maneira de ser menor ou igual a outro objeto. O que nos leva a esta definição do objeto inicial:

<em>O **objeto inicial** é o objeto que possui um e apenas um morfismo que vai para qualquer objeto da categoria.</em>
![](images/image_5.jpg)

No entanto, mesmo isso não garante a exclusividade do objeto inicial (se houver). Mas isso garante a segunda melhor coisa: a exclusividade até o isomorfismo. Os isomorfismos são muito importantes na teoria das categorias, então falarei sobre eles em breve. Por enquanto, vamos apenas concordar que a exclusividade até o isomorfismo justifica o uso de "o" na definição do objeto inicial.

Aqui estão alguns exemplos: O objeto inicial em um conjunto parcialmente ordenado (geralmente chamado de poset) é seu menor elemento. Alguns posets não têm um objeto inicial - como o conjunto de todos os inteiros, positivos e negativos, com relação menor ou igual para morfismos.

Na categoria de conjuntos e funções, o objeto inicial é o conjunto vazio. Lembre-se, um conjunto vazio corresponde ao tipo `Void` de Haskell (não há nenhum tipo correspondente em C++) e a função polimórfica única de `Void` para qualquer outro tipo é chamada de `absurd`:
```hs
absurd :: Void -> a
```

É essa família de morfismos que torna o `Void` o objeto inicial na categoria de tipos.

#### Objeto terminal
Vamos continuar com o padrão de objeto único, mas vamos mudar a maneira como classificamos os objetos. Diremos que o objeto `a` é "mais terminal" do que o objeto `b` se houver um morfismo indo de `b` para `a` (observe a inversão de direção). Estaremos procurando por um objeto que seja mais terminal do que qualquer outro objeto da categoria. Novamente, vamos insistir na exclusividade:

<em>O **objeto terminal** é o objeto com um e apenas um morfismo vindo de qualquer objeto da categoria.</em>
![](images/image_6.jpg)

E, novamente, o objeto terminal é único, até o isomorfismo, que mostrarei em breve. Mas primeiro vamos ver alguns exemplos. Em um poset, o objeto terminal, se existir, é o maior objeto. Na categoria de conjuntos, o objeto terminal é um singleton. Já falamos sobre singletons - eles correspondem ao tipo `void` em C++ e ao tipo de unidade `()` em Haskell. É um tipo que tem apenas um valor - implícito em C++ e explícito em Haskell, denotado por `()`. Também estabelecemos que existe uma e apenas uma função pura de qualquer tipo para o tipo de unidade:
```hs
unit :: a -> ()
unit _ = ()
```

portanto, todas as condições para o objeto terminal são satisfeitas.

Observe que, neste exemplo, a condição de exclusividade é crucial, porque existem outros conjuntos (na verdade, todos eles, exceto o conjunto vazio) que têm morfismos de entrada de cada conjunto. Por exemplo, existe uma função com valor booleano (um predicado) definida para cada tipo:
```hs
yes :: a -> Bool
yes _ = True
```

Mas `Bool` não é um objeto terminal. Há pelo menos mais uma função com valor `Bool` de cada tipo:
```hs
no :: a -> Bool
no _ = False
```

A insistência na exclusividade nos dá a precisão certa para restringir a definição do objeto terminal a apenas um tipo.

#### Dualidade
Você não pode deixar de notar a simetria entre a maneira como definimos o objeto inicial e o objeto terminal. A única diferença entre os dois era a direção dos morfismos. Acontece que, para qualquer categoria C, podemos definir a categoria oposta $`C^{OP}`$ apenas invertendo todas as setas. A categoria oposta satisfaz automaticamente todos os requisitos de uma categoria, desde que redefinamos simultaneamente a composição. Se os morfismos originais `f :: a -> b` e `g :: b -> c` compostos a `h :: a -> c` com `h = g∘f`, então os morfismos invertidos `fop :: b -> a` e `gop :: c -> b` irá compor para `hop :: c -> a` com `hop = fop∘gop`. E inverter as setas de identidade é um (alerta de trocadilho!) no-op.

A dualidade é uma propriedade muito importante das categorias porque dobra a produtividade de todo matemático que trabalha na teoria das categorias. Para cada construção que você propõe, existe o seu oposto; e para cada teorema que você prova, obtém um de graça. As construções na categoria oposta geralmente são prefixadas com “co”, então você tem produtos e coprodutos, mônadas e comônadas, cones e cocones, limites e colimites e assim por diante. Porém, não há cocomônadas, porque inverter as setas duas vezes nos leva de volta ao estado original.

Segue-se então que um objeto terminal é o objeto inicial na categoria oposta.

#### Isomorfismos
Como programadores, estamos bem cientes de que definir igualdade não é uma tarefa trivial. O que significa dois objetos serem iguais? Eles têm que ocupar o mesmo local na memória (igualdade de ponteiro)? Ou é suficiente que os valores de todos os seus componentes sejam iguais? Dois números complexos são iguais se um for expresso como a parte real e imaginária e o outro como módulo e ângulo? Você pensaria que os matemáticos teriam descoberto o significado de igualdade, mas não o fizeram. Eles têm o mesmo problema de múltiplas definições concorrentes para igualdade. Existe a igualdade proposicional, igualdade intensional, igualdade extensional e igualdade como um caminho na teoria dos tipos de homotopia. E então existem as noções mais fracas de isomorfismo e ainda mais fracas de equivalência.

A intuição é que os objetos isomórficos têm a mesma aparência - eles têm a mesma forma. Isso significa que cada parte de um objeto corresponde a alguma parte de outro objeto em um mapeamento um-para-um. Até onde nossos instrumentos podem dizer, os dois objetos são uma cópia perfeita um do outro. Matematicamente, significa que há um mapeamento do objeto `a` para o objeto `b`, e há um mapeamento do objeto `b` de volta para o objeto `a`, e eles são o inverso um do outro. Na teoria das categorias, substituímos os mapeamentos por morfismos. Um isomorfismo é um morfismo invertível; ou um par de morfismos, um sendo o inverso do outro.

Entendemos o inverso em termos de composição e identidade: o morfismo `g` é o inverso do morfismo `f` se sua composição é o morfismo de identidade. Na verdade, essas são duas equações porque existem duas maneiras de compor dois morfismos:
```hs
f . g = id
g . f = id
```

Quando eu disse que o objeto inicial (terminal) era único até o isomorfismo, quis dizer que quaisquer dois objetos iniciais (terminais) são isomórficos. Isso é realmente fácil de ver. Suponhamos que temos dois objetos iniciais i1 e i2. Como i1 é inicial, existe um morfismo único `f` de i1 a i2. Da mesma forma, como i2 é inicial, existe um morfismo único `g` de i2 a i1. Qual é a composição desses dois morfismos?

![](images/image_7.jpg)

<em>Todos os morfismos neste diagrama são únicos</em>

A composição `g∘f` deve ser um morfismo de i1 a i1. Mas i1 é inicial, então só pode haver um morfismo indo de i1 a i1. Como estamos em uma categoria, sabemos que há um morfismo de identidade de i1 a i1 e, como só há espaço para uma, deve ser isso. Portanto, g∘f é igual a identidade. Da mesma forma, f∘g deve ser igual à identidade, porque só pode haver um morfismo de i2 de volta a i2. Isso prova que feg devem ser o inverso um do outro. Portanto, quaisquer dois objetos iniciais são isomórficos.

Observe que nesta prova usamos a unicidade do morfismo do objeto inicial para ele mesmo. Sem isso, não poderíamos provar a parte "até o isomorfismo". Mas por que precisamos da exclusividade de `f` e `g`? Porque não apenas o objeto inicial é único até o isomorfismo, ele é único até o isomorfismo único. Em princípio, pode haver mais de um isomorfismo entre dois objetos, mas esse não é o caso aqui. Essa “singularidade até o isomorfismo único” é a propriedade importante de todas as construções universais.

#### Produtos
A próxima construção universal é a de um produto. Sabemos o que é um produto cartesiano de dois conjuntos: é um conjunto de pares. Mas qual é o padrão que conecta o conjunto de produtos com seus conjuntos constituintes? Se pudermos descobrir isso, seremos capazes de generalizá-lo para outras categorias.

Tudo o que podemos dizer é que existem duas funções, as projeções, do produto para cada um dos constituintes. Em Haskell, essas duas funções são chamadas de `fst` e `snd` e selecionam, respectivamente, o primeiro e o segundo componentes de um par:
```hs
fst :: (a, b) -> a
fst (x, y) = x

snd :: (a, b) -> b
snd (x, y) = y
```

Aqui, as funções são definidas pelo padrão de correspondência de seus argumentos: o padrão que corresponde a qualquer par é `(x, y)` e extrai seus componentes nas variáveis `x` e `y`.

Essas definições podem ser simplificadas ainda mais com o uso de curingas:
```hs
fst (x, _) = x
snd (_, y) = y
```

Em C++, usaríamos funções template, por exemplo:
```cpp
template<class A, class B>
A fst(pair<A, B> const & p) {
    return p.first;
}
```

Munidos desse conhecimento aparentemente muito limitado, vamos tentar definir um padrão de objetos e morfismos na categoria de conjuntos que nos levará à construção de um produto de dois conjuntos, `a` e `b`. Este padrão consiste em um objeto `c` e dois morfismos `p` e `q` conectando-o a `a` e `b`, respectivamente:
```hs
p :: c -> a
q :: c -> b
```
![](images/image_8.jpg)

Todos os cs que se enquadrarem nesse padrão serão considerados candidatos ao produto. Pode haver muitos deles.
![](images/image_9.jpg)

Por exemplo, vamos escolher, como nossos constituintes, dois tipos Haskell, `Int` e `Bool`, e obter uma amostra de candidatos para seus produtos.

Aqui está um: `Int`. `Int` pode ser considerado um candidato para o produto de `Int` e `Bool`? Sim, pode - e aqui estão suas projeções:
```hs
p :: Int -> Int
p x = x

q :: Int -> Bool
q _ = True
```

Isso é muito chato, mas atende aos critérios.

Aqui está outro: `(Int, Int, Bool)`. É uma tupla de três elementos, ou um triplo. Aqui estão dois morfismos que o tornam um candidato legítimo (estamos usando correspondência de padrões em triplos):
```hs
p :: (Int, Int, Bool) -> Int
p (x, _, _) = x

q :: (Int, Int, Bool) -> Bool
q (_, _, b) = b
```

Você deve ter notado que, embora nosso primeiro candidato fosse muito pequeno, ele cobria apenas a dimensão `Int` do produto; o segundo era muito grande - duplicou espúriamente a dimensão `Int`.

Mas ainda não exploramos a outra parte da construção universal: o ranking. Queremos ser capazes de comparar duas instâncias do nosso padrão. Queremos comparar um objeto candidato `c` e suas duas projeções `p` e `q` com outro objeto candidato `c'` e suas duas projeções `p'` e `q'`. Gostaríamos de dizer que `c` é "melhor" do que `c'` se houver um morfismo `m` de `c'` para `c` - mas isso é muito fraco. Também queremos que suas projeções sejam "melhores" ou "mais universais" do que as projeções de `c'`. O que isso significa é que as projeções `p'` e `q'` podem ser reconstruídas a partir de `p` e `q` usando `m`:
```hs
p' = p . m
q' = q . m
```

Outra maneira de olhar para essas equações é que `m` fatora `p'` e `q'`. Basta fingir que essas equações estão em números naturais e o ponto é a multiplicação: `m` é um fator comum compartilhado por `p'` e `q'`.

Apenas para construir algumas intuições, deixe-me mostrar que o par `(Int, Bool)` com as duas projeções canônicas, `fst` e `snd` é de fato melhor do que os dois candidatos que apresentei antes.

![](images/image_10.jpg)

O mapeamento `m` para o primeiro candidato é:
```hs
m :: Int -> (Int, Bool)
m x = (x, True)
```

Na verdade, as duas projeções, `p` e `q` podem ser reconstruídas como:
```hs
p x = fst (m x) = x
q x = snd (m x) = True
```

O `m` para o segundo exemplo é determinado de maneira semelhante e única:
```hs
m (x, _, b) = (x, b)
```

Pudemos mostrar que `(Int, Bool)` é melhor do que qualquer um dos dois candidatos. Vamos ver por que o oposto não é verdade. Podemos encontrar algum `m'` que nos ajude a reconstruir `fst` e `snd` a partir de `p` e `q`?
```hs
fst = p . m'
snd = q . m'
```

Em nosso primeiro exemplo, `q` sempre retornou `True` e sabemos que existem pares cujo segundo componente é `False`. Não podemos reconstruir `snd` a partir de `q`.

O segundo exemplo é diferente: retemos informações suficientes após executar `p` ou `q`, mas há mais de uma maneira de fatorar `fst` e `snd`. Porque `p` e `q` ignoram o segundo componente do triplo, nosso `m'` pode colocar qualquer coisa nele. Nós podemos ter:
```hs
m' (x, b) = (x, x, b)
```

ou
```hs
m' (x, b) = (x, 42, b)
```

e assim por diante.

Juntando tudo isso, dado qualquer tipo `c` com duas projeções `p` e `q`, existe um único `m` de c para o produto cartesiano `(a, b)` que os fatora. Na verdade, ele apenas combina `p` e `q` em um par.
```hs
m :: c -> (a, b)
m x = (p x, q x)
```

Isso faz do produto cartesiano `(a, b)` nossa melhor combinação, o que significa que essa construção universal funciona na categoria de conjuntos. Ele escolhe o produto de quaisquer dois conjuntos.

Agora vamos esquecer os conjuntos e definir um produto de dois objetos em qualquer categoria usando a mesma construção universal. Esse produto nem sempre existe, mas quando existe, é único até um isomorfismo único.

Um **produto** de dois objetos `a` e `b` é o objeto `c` equipado com duas projeções de modo que para qualquer outro objeto `c'` equipado com duas projeções, há um morfismo único `m` de `c'` para `c` que fatoriza essas projeções.

Uma função (de ordem superior) que produz a função de fatoração `m` a partir de dois candidatos é às vezes chamada de fatorador. No nosso caso, seria a função:
```hs
factorizer :: (c -> a) -> (c -> b) -> (c -> (a, b))
factorizer p q = \x -> (p x, q x)
```

#### Coproduto
Como toda construção na teoria das categorias, o produto possui um dual, que é denominado coproduto. Quando invertemos as setas no padrão do produto, terminamos com um objeto `c` equipado com duas injeções, `i` e `j`: morfismos de `a` e `b` para `c`.
```hs
i :: a -> c
j :: b -> c
```

![](images/image_11.jpg)

A classificação também é invertida: o objeto `c` é "melhor" do que o objeto `c'` que está equipado com as injeções `i'` e `j'` se houver um morfismo `m` de `c` para `c'` que fatora as injeções:
```hs
i' = m . i
j' = m . j
```

O “melhor” tal objeto, aquele com um morfismo único conectando-o a qualquer outro padrão, é chamado de coproduto e, se existir, é único até um isomorfismo único.

>Um **coproduto** de dois objetos `a` e `b` é o objeto `c` equipado com duas injeções de modo que para qualquer outro objeto `c'` equipado com duas injeções, há um morfismo único `m` de `c` para `c'` que fatoriza essas injeções.

Na categoria de conjuntos, o coproduto é a união disjunta de dois conjuntos. Um elemento da união disjunta de `a` e `b` é um elemento de `a` ou um elemento de `b`. Se os dois conjuntos se sobrepõem, a união disjunta contém duas cópias da peça comum. Você pode pensar em um elemento de uma união disjunta como sendo marcado com um identificador que especifica sua origem.

Para um programador, é mais fácil entender um coproduto em termos de tipos: é uma união marcada de dois tipos. C++ oferece suporte a uniões, mas elas não são marcadas. Isso significa que, em seu programa, você deve, de alguma forma, controlar qual membro da união é válido. Para criar uma união marcada, você deve definir uma marca - uma enumeração - e combiná-la com a união. Por exemplo, uma união marcada de um `int` e um `char const *` pode ser implementada como:
```cpp
struct Contact {
    enum { isPhone, isEmail } tag;
    union { int phoneNum; char const * emailAddr; };
};
```

As duas injeções podem ser implementadas como construtores ou como funções. Por exemplo, aqui está a primeira injeção como uma função `PhoneNum`:
```cpp
Contact PhoneNum(int n) {
    Contact c;
    c.tag = isPhone;
    c.phoneNum = n;
    return c;
}
```

Ele injeta um número inteiro no `Contact`.

Uma união marcada também é chamada de variante, e há uma implementação muito geral de uma variante na biblioteca boost, `boost::variant`.

Em Haskell, você pode combinar quaisquer tipos de dados em uma união marcada, separando os construtores de dados com uma barra vertical. O exemplo de `Contact` se traduz na declaração:
```hs
data Contact = PhoneNum Int | EmailAddr String
```

Aqui, `PhoneNum` e `EmailAddr` servem como construtores (injeções) e como tags para correspondência de padrões (mais sobre isso posteriormente). Por exemplo, é assim que você construiria um contato usando um número de telefone:
```hs
helpdesk :: Contact
helpdesk = PhoneNum 2222222
```

Ao contrário da implementação canônica do produto que é construída em Haskell como o par primitivo, a implementação canônica do coproduto é um tipo de dados chamado `Either`, que é definido na Prelude padrão como:
```hs
Either a b = Left a | Right b
```

É parametrizado por dois tipos, `a` e `b`, e tem dois construtores: `Left`, que recebe um valor do tipo `a`, e `Right`, que usa um valor do tipo `b`.

Assim como definimos o fatorizador para um produto, podemos definir um para o coproduto. Dado um tipo candidato `c` e duas injeções candidatas `i` e `j`, o fatorador para `Either` produz a função de fatoração:
```hs
factorizer :: (a -> c) -> (b -> c) -> Either a b -> c
factorizer i j (Left a)  = i a
factorizer i j (Right b) = j b
```

#### Assimetria
Vimos dois conjuntos de definições duais: A definição de um objeto terminal pode ser obtida a partir da definição do objeto inicial invertendo a direção das setas; da mesma forma, a definição do coproduto pode ser obtida a partir da do produto. Ainda assim, na categoria de conjuntos, o objeto inicial é muito diferente do objeto final, e o coproduto é muito diferente do produto. Veremos mais tarde que o produto se comporta como uma multiplicação, com o objeto terminal desempenhando o papel de um; enquanto o coproduto se comporta mais como a soma, com o objeto inicial desempenhando o papel de zero. Em particular, para conjuntos finitos, o tamanho do produto é o produto dos tamanhos de conjuntos individuais, e o tamanho do coproduto é a soma dos tamanhos.

Isso mostra que a categoria dos conjuntos não é simétrica em relação à inversão das setas.

Observe que, embora o conjunto vazio tenha um morfismo único para qualquer conjunto (a função absurda), ele não tem morfismos de volta. O conjunto de singleton tem um morfismo único vindo de qualquer conjunto, mas também tem morfismos de saída para cada conjunto (exceto para o vazio). Como vimos antes, esses morfismos de saída do objeto terminal desempenham um papel muito importante de escolher elementos de outros conjuntos (o conjunto vazio não tem elementos, então não há nada para escolher).

É a relação do conjunto singleton com o produto que o diferencia do coproduto. Considere usar o conjunto singleton, representado pelo tipo de unidade `()`, como mais um candidato - muito inferior - para o padrão de produto. Equipe-o com duas projeções peq: funções do singleton para cada um dos conjuntos constituintes. Cada um seleciona um elemento concreto de qualquer conjunto. Como o produto é universal, há também um morfismo (único) `m` de nosso candidato, o singleton, para o produto. Este morfismo seleciona um elemento do conjunto de produtos - ele seleciona um par concreto. Ele também fatoriza as duas projeções:
```hs
p = fst. m
q = snd. m
```

Ao agir sobre o valor singleton `()`, o único elemento do conjunto singleton, essas duas equações tornam-se:
```hs
p () = fst (m ())
q () = snd (m ())
```

Como `m ()` é o elemento do produto escolhido por `m`, essas equações nos dizem que o elemento escolhido por `p` do primeiro conjunto, `p ()`, é o primeiro componente do par escolhido por `m`. Da mesma forma, `q ()` é igual ao segundo componente. Isso está em total acordo com nosso entendimento de que os elementos do produto são pares de elementos dos conjuntos constituintes.

Não existe uma interpretação tão simples do coproduto. Poderíamos tentar o conjunto singleton como candidato a um coproduto, na tentativa de extrair os elementos dele, mas aí teríamos duas injeções entrando nele em vez de duas projeções saindo dele. Eles não nos disseram nada sobre suas fontes (na verdade, vimos que eles ignoram o parâmetro de entrada). Nem o morfismo único do coproduto ao nosso singleton. A categoria de conjuntos parece muito diferente quando vista da direção do objeto inicial do que quando vista da extremidade do terminal.

Esta não é uma propriedade intrínseca de conjuntos, é uma propriedade de funções, que usamos como morfismos em `Set`. As funções são, em geral, assimétricas. Deixe-me explicar.

Uma função deve ser definida para cada elemento de seu conjunto de domínio (na programação, chamamos de função total), mas não precisa cobrir todo o codomínio. Vimos alguns casos extremos disso: funções de um conjunto singleton - funções que selecionam apenas um único elemento no codomínio. (Na verdade, as funções de um conjunto vazio são os extremos reais.) Quando o tamanho do domínio é muito menor do que o tamanho do codomínio, geralmente pensamos em tais funções como embutir o domínio no codomínio. Por exemplo, podemos pensar em uma função de um conjunto singleton como incorporando seu único elemento no codomínio. Eu as chamo de funções de incorporação, mas os matemáticos preferem dar um nome ao oposto: as funções que preenchem seus codomínios são chamadas de sobrejetivas ou sobre.

A outra fonte de assimetria é que as funções podem mapear muitos elementos do conjunto de domínio em um elemento do codomínio. Eles podem derrubá-los. O caso extremo são funções que mapeiam conjuntos inteiros em um singleton. Você viu a função da unidade polimórfica que faz exatamente isso. O colapso só pode ser agravado pela composição. Uma composição de duas funções em colapso é ainda mais colapsante do que as funções individuais. Os matemáticos têm um nome para funções de não colapso: eles as chamam de injetivas ou um-para-um

É claro que existem algumas funções que não são incorporadas nem colapsadas. Eles são chamados de bijeções e são verdadeiramente simétricos, porque são invertíveis. Na categoria de conjuntos, um isomorfismo é o mesmo que uma bijeção.

#### Desafios
1. Mostre que o objeto terminal é único até um isomorfismo único.
2. O que é um produto de dois objetos em um poset? Dica: use a construção universal.
3. O que é um coproduto de dois objetos em um poset?
4. Implemente o equivalente a Haskell ou como um tipo genérico em sua linguagem favorita (diferente de Haskell).
5. Mostre que qualquer `Either` é um coproduto "melhor" do que o `int` equipado com duas injeções:
```cpp
int i(int n) { return n; }
int j(bool b) { return b? 0: 1; }
```
Dica: defina uma função
```cpp
int m(Either const & e);
```
que fatoriza `i` e `j`.
6. Continuando com o problema anterior: como você argumentaria que `int` com as duas injeções `i` e `j` não pode ser “melhor” do que `Either`?
7. Continuando: E quanto a essas injeções?
```cpp
int i(int n) { 
    if (n < 0) return n; 
    return n + 2;
}
int j(bool b) { return b? 0: 1; }
```
8. Encontre um candidato inferior para um coproduto de `int` e `bool` que não possa ser melhor do que `Either` porque permite vários morfismos aceitáveis dele para `Either`.

#### Bibliografia
- O vídeo do Catsters, [Products and Coproducts](https://www.youtube.com/watch?v=upCSDIO9pjc).

#### Agradecimentos
Sou grato a Gershom Bazerman por revisar este post antes da publicação e por estimular as discussões.

### Tipos de dados algébricos simples
Vimos duas maneiras básicas de combinar tipos: usando um produto e um coproduto. Acontece que muitas estruturas de dados na programação diária podem ser construídas usando apenas esses dois mecanismos. Esse fato tem consequências práticas importantes. Muitas propriedades de estruturas de dados são combináveis. Por exemplo, se você sabe como comparar valores de tipos básicos para igualdade e sabe como generalizar essas comparações para tipos de produto e coproduto, pode automatizar a derivação de operadores de igualdade para tipos compostos. Em Haskell, você pode derivar automaticamente igualdade, comparação, conversão de e para string, e muito mais, para um grande subconjunto de tipos compostos.

Vamos dar uma olhada mais de perto nos tipos de produto e soma conforme aparecem na programação.

#### Product type
A implementação canônica de um produto de dois tipos em uma linguagem de programação é um par. Em Haskell, um par é um construtor de tipo primitivo; em C++ é um modelo relativamente complexo definido na Biblioteca Padrão.

![](images/image_12.jpg)

Os pares não são estritamente comutativos: um par `(Int, Bool)` não pode ser substituído por um par `(Bool, Int)`, embora contenham a mesma informação. Eles são, no entanto, comutativos até o isomorfismo - o isomorfismo sendo dado pela função de troca (que é seu próprio inverso):
```hs
swap :: (a, b) -> (b, a)
swap (x, y) = (y, x)
```

Você pode pensar nos dois pares simplesmente usando um formato diferente para armazenar os mesmos dados. É como big endian vs. little endian.

Você pode combinar um número arbitrário de tipos em um produto aninhando pares dentro de pares, mas há uma maneira mais fácil: pares aninhados são equivalentes a tuplas. É a consequência do fato de que diferentes formas de pares de aninhamento são isomórficas. Se você deseja combinar três tipos em um produto, `a`, `b` e `c`, nesta ordem, você pode fazer isso de duas maneiras:
```hs
((a, b), c)
```

ou

```hs
(a, (b, c))
```

Esses tipos são diferentes - você não pode passar um para uma função que espera o outro - mas seus elementos estão em correspondência um a um. Existe uma função que mapeia um para o outro:
```hs
alpha :: ((a, b), c) -> (a, (b, c))
alpha ((x, y), z) = (x, (y, z))
```

e esta função é invertível:
```hs
alpha_inv :: (a, (b, c)) -> ((a, b), c)
alpha_inv  (x, (y, z)) = ((x, y), z)
```

então é um isomorfismo. Essas são apenas maneiras diferentes de reempacotar os mesmos dados.

Você pode interpretar a criação de um product type como uma operação binária em tipos. Dessa perspectiva, o isomorfismo acima se parece muito com a lei da associatividade que vimos nos monóides:
```hs
(a * b) * c = a * (b * c)
```

Exceto que, no caso do monóide, as duas formas de composição dos produtos eram iguais, enquanto aqui são apenas iguais “até o isomorfismo”.

Se pudermos conviver com isomorfismos, e não insistir na igualdade estrita, podemos ir ainda mais longe e mostrar que o tipo de unidade, `()`, é a unidade do produto da mesma forma que 1 é a unidade de multiplicação. Na verdade, o emparelhamento de um valor de algum tipo `a` com uma unidade não adiciona nenhuma informação. O tipo:
```hs
(a, ())
```

é isomórfico a `a`. Aqui está o isomorfismo:
```hs
rho :: (a, ()) -> a
rho (x, ()) = x

rho_inv :: a -> (a, ())
rho_inv x = (x, ())
```

Essas observações podem ser formalizadas dizendo que `Set` (a categoria dos conjuntos) é uma categoria monoidal. É uma categoria que também é monóide, no sentido de que você pode multiplicar objetos (aqui, pegue seu produto cartesiano). Vou falar mais sobre categorias monoidais e dar a definição completa no futuro.

Existe uma maneira mais geral de definir product types em Haskell - especialmente, como veremos em breve, quando eles são combinados com sum types. Ele usa construtores nomeados com vários argumentos. Um par, por exemplo, pode ser definido alternativamente como:
```hs
data Pair a b = P a b
```

Aqui, `Pair a b` é o nome do tipo paremetrizado por dois outros tipos, `a` e `b`; e `P` é o nome do construtor de dados. Você define um tipo de par passando dois tipos para o construtor de tipo `Pair`. Você constrói um valor de par passando dois valores de tipos apropriados para o construtor `P`. Por exemplo, vamos definir um valor `stmt` como um par de `String` e `Bool`:
```hs
stmt :: Pair String Bool
stmt = P "Essa afirmação é" False
```

A primeira linha é a declaração do tipo. Ele usa o construtor de tipo `Pair`, com `String` e `Bool` substituindo `a` e `b` na definição genérica de `Pair`. A segunda linha define o valor real passando uma string concreta e um booleano concreto para o construtor de dados `P`. Os construtores de tipo são usados para construir tipos; construtores de dados, para construir valores.

Como os espaços de nome para construtores de tipo e dados são separados em Haskell, você verá frequentemente o mesmo nome usado para ambos, como em:
```hs
data Pair a b = Pair a b
```

E se você apertar os olhos com força suficiente, poderá até ver o tipo de par embutido como uma variação desse tipo de declaração, em que o nome `Pair` é substituído pelo operador binário `(,)`. Na verdade, você pode usar `(,)` como qualquer outro construtor nomeado e criar pares usando a notação de prefixo:
```hs
stmt = (,) "Esta afirmação é" False
```

Da mesma forma, você pode usar `(,,)` para criar triplos e assim por diante.

Em vez de usar pares ou tuplas genéricas, você também pode definir tipos de produtos nomeados específicos, como em:
```hs
data Stmt = Stmt String Bool
```

que é apenas um produto de `String` e `Bool`, mas tem seu próprio nome e construtor. A vantagem desse estilo de declaração é que você pode definir muitos tipos que têm o mesmo conteúdo, mas significados e funcionalidades diferentes, e que não podem ser substituídos uns pelos outros.

Programar com tuplas e construtores de vários argumentos pode se tornar confuso e sujeito a erros - manter o controle de qual componente representa o quê. Muitas vezes é preferível dar nomes aos componentes. Um product type com campos nomeados é chamado de record em Haskell e struct em C.

#### Records
Vamos dar uma olhada em um exemplo simples. Queremos descrever os elementos químicos combinando duas strings, nome e símbolo; e um inteiro, o número atômico; em uma estrutura de dados. Podemos usar uma tupla `(String, String, Int)` e lembrar qual componente representa o quê. Extrairíamos os componentes por correspondência de padrões, como nesta função que verifica se o símbolo do elemento é o prefixo de seu nome (como em **He** sendo o prefixo de **Hélio**):
```hs
startsWithSymbol :: (String, String, Int) -> Bool
startsWithSymbol (name, symbol, _) = isPrefixOf symbol name
```

Este código está sujeito a erros e é difícil de ler e manter. É muito melhor definir um record:
```hs
data Element = Element { name         :: String
                       , symbol       :: String
                       , atomicNumber :: Int }
```

As duas representações são isomórficas, como testemunhado por essas duas funções de conversão, que são o inverso uma da outra:
```hs
tupleToElem :: (String, String, Int) -> Element
tupleToElem (n, s, a) = Element { name         = n
                                , symbol       = s
                                , atomicNumber = a }

elemToTuple :: Element -> (String, String, Int)
elemToTuple e = (name e, symbol e, atomicNumber e)
```

Observe que os nomes dos campos de registro também servem como funções para acessar esses campos. Por exemplo, `atomicNumber e` pega o campo `atomicNumber` de `e`. Usamos `atomicNumber` como uma função do tipo:
```hs
atomicNumber :: Element -> Int
```

Com a sintaxe de record para `Element`, nossa função `startsWithSymbol` se torna mais legível:
```hs
startsWithSymbol :: Element -> Bool
startWithSymbol e = isPrefixOf (symbol e) (name e)
```

Poderíamos até usar o truque de Haskell para transformar a função `isPrefixOf` em um operador infixo, cercando-o com aspas e fazê-lo parecer quase uma frase:
```hs
startWithSymbol e = symbol e name `isPrefixOf` e
```

Os parênteses podem ser omitidos neste caso, porque um operador infixo tem precedência mais baixa do que uma chamada de função.

#### Sum types
Assim como o produto na categoria de conjuntos dá origem a product types, o coproduto dá origem a sum types. A implementação canônica de um sum type em Haskell é:
```hs
data Either a b = Left a | Right b
```

E como os pares, os `Either`s são comutativos (até o isomorfismo), podem ser aninhados e a ordem de aninhamento é irrelevante (até o isomorfismo). Portanto, podemos, por exemplo, definir uma soma equivalente a um triplo:
```hs
data OneOfThree a b c = Sinistral a | Medial b | Dextral c
```

e assim por diante.

Acontece que `Set` também é uma categoria monoidal (simétrica) com respeito ao coproduto. O papel da operação binária é desempenhado pela soma disjunta, e o papel do elemento de unidade é desempenhado pelo objeto inicial. Em termos de tipos, temos `Either` como operador monoidal e `Void`, o tipo desabitado, como seu elemento neutro. Você pode pensar em `Either` como mais (+), e `Void` como zero. Na verdade, adicionar `Void` a um sum type não altera seu conteúdo. Por exemplo:
```hs
Either a Void
```

é isomórfico a `a`. Isso ocorre porque não há como construir uma versão certa desse tipo - não há um valor do tipo `Void`. Os únicos habitantes de `Either a Void` são construídos usando os construtores `Left` e eles simplesmente encapsulam um valor do tipo `a`. Então, simbolicamente, `a + 0 = a`.

Os sum types são bastante comuns em Haskell, mas seus equivalentes C++, unions ou variants, são muito menos comuns. Há várias razões para isso.

Em primeiro lugar, os sum types mais simples são apenas enumerações e são implementados usando `enum` em C++. O equivalente ao sum type em Haskell:
```hs
data Color = Red | Green | Blue
```

é em C++:
```cpp
enum { Red, Green, Blue };
```
Um sum type ainda mais simples:
```hs
data Bool = True | False
```

é o `bool` primitivo em C++.

Sum types simples que codificam a presença ou ausência de um valor são implementados de várias maneiras em C++ usando truques especiais e valores "impossíveis", como strings vazias, números negativos, ponteiros nulos, etc. Este tipo de opcionalidade, se deliberada, é expressa em Haskell usando o tipo `Maybe`:
```hs
data Maybe a = Nothing | Just a
```

O tipo `Maybe` é uma soma de dois tipos. Você pode ver isso se separar os dois construtores em tipos individuais. O primeiro seria assim:
```hs
data NothingType = Nothing
```

É uma enumeração com um valor chamado `Nothing`. Em outras palavras, é um singleton, que é equivalente ao tipo de unidade `()`. A segunda parte:
```hs
data JustType a = Just a
```

é apenas um encapsulamento do tipo a. Poderíamos ter codificado `Maybe` como:
```hs
data Maybe a = Either () a
```

Sum types mais complexos são frequentemente falsificados em C++ usando ponteiros. Um ponteiro pode ser nulo ou apontar para um valor de tipo específico. Por exemplo, um tipo de lista Haskell, que pode ser definido como um sum type (recursivo):
```hs
data List a = Nil | Cons a (List a)
```

pode ser traduzido para C++ usando o truque do ponteiro nulo para implementar a lista vazia:
```cpp
template<class A> 
class List {
    Node<A> * _head;
public:
    List() : _head(nullptr) {}  // Nil
    List(A a, List<A> l)        // Cons
      : _head(new Node<A>(a, l))
    {}
};
```

Observe que os dois construtores Haskell `Nil` e `Cons` são traduzidos em dois construtores `List` overloadeds com argumentos análogos (nenhum, para `Nil`; e um valor e uma lista para `Cons`). A classe `List` não precisa de uma tag para distinguir entre os dois componentes do sum type. Em vez disso, ele usa o valor `nullptr` especial para `_head` para codificar `Nil`.

A principal diferença, entretanto, entre os tipos Haskell e C++ é que as estruturas de dados Haskell são imutáveis. Se você criar um objeto usando um construtor específico, o objeto lembrará para sempre qual construtor foi usado e quais argumentos foram passados ​​para ele. Portanto, um objeto `Maybe` criado como `Just "energy"` nunca se tornará `Nothing`. Da mesma forma, uma lista vazia ficará para sempre vazia e uma lista de três elementos sempre terá os mesmos três elementos.

É essa imutabilidade que torna a construção reversível. Dado um objeto, você sempre pode desmontá-lo em partes que foram usadas em sua construção. Essa desconstrução é feita com correspondência de padrões e reutiliza construtores como padrões. Os argumentos do construtor, se houver, são substituídos por variáveis ​​(ou outros padrões).

O tipo de dados `List` tem dois construtores, portanto, a desconstrução de uma `List` arbitrária usa dois padrões correspondentes a esses construtores. Um corresponde à lista vazia de `Nil` e o outro a uma lista construída por `Cons`. Por exemplo, aqui está a definição de uma função simples em `List`:
```hs
maybeTail :: List a -> Maybe (List a)
maybeTail Nil = Nothing
maybeTail (Cons _ t) = Just t
```

A primeira parte da definição de `MaybeTail` usa o construtor `Nil` como padrão e retorna `Nothing`. A segunda parte usa o construtor `Cons` como padrão. Ele substitui o primeiro argumento do construtor por um curinga, porque não estamos interessados ​​nele. O segundo argumento para `Cons` está ligado à variável `t` (chamarei essas coisas de variáveis ​​mesmo que, estritamente falando, elas nunca variem: uma vez ligada a uma expressão, uma variável nunca muda). O valor de retorno é `Just t`. Agora, dependendo de como sua lista foi criada, ela corresponderá a uma das cláusulas. Se ela foi criada usando `Cons`, os dois argumentos que foram passados ​​para ela serão recuperados (e o primeiro descartado).

Sum types ainda mais elaborados são implementados em C++ usando hierarquias de classes polimórficas. Uma família de classes com um ancestral comum pode ser entendida como um tipo variante, em que a vtable serve como uma tag oculta. O que em Haskell seria feito por correspondência de padrões no construtor e chamando código especializado, em C++ é realizado despachando uma chamada para uma função virtual baseada no ponteiro vtable.

Você raramente verá `union` usada como um sum type em C++ por causa das limitações severas sobre o que pode entrar em uma `union`. Você não pode nem mesmo colocar uma `std::string` em uma `union` porque ela tem um construtor de cópia.

#### Álgebra de tipos
Tomados separadamente, os product e sum types podem ser usados ​​para definir uma variedade de estruturas de dados úteis, mas a força real vem da combinação dos dois. Mais uma vez, estamos invocando o poder de composição.

Vamos resumir o que descobrimos até agora. Vimos duas estruturas monoidais comutativas subjacentes ao sistema de tipo: temos os tipos de soma com `Void` como o elemento neutro e os tipos de produto com o tipo de unidade, `()`, como o elemento neutro. Gostaríamos de pensar neles como análogos à adição e multiplicação. Nesta analogia, `Void` corresponderia a zero, e unidade, `()`, a um.

Vamos ver até onde podemos estender essa analogia. Por exemplo, a multiplicação por zero resulta em zero? Em outras palavras, um product type com um componente sendo vazio é isomórfico para vazio? Por exemplo, é possível criar um par de, digamos, `Int` e `Void`?

Para criar um par, você precisa de dois valores. Embora você possa facilmente chegar a um número inteiro, não há valor do tipo `Void`. Portanto, para qualquer tipo `a`, o tipo `(a, Void)` é desabitado - não tem valores - e é, portanto, equivalente a `Void`. Em outras palavras, `a * 0 = 0`.

Outra coisa que liga adição e multiplicação é a propriedade distributiva:
```
a * (b + c) = a * b + a * c
```

Isso também é válido para os product e sum types? Sim, é verdade - até isomorfismos, como de costume. O lado esquerdo corresponde ao tipo:
```hs
(a, Either b c)
```

e o lado direito corresponde ao tipo:
```hs
Either (a, b) (a, c)
```

Esta é a função que os converte de uma maneira:
```hs
prodToSum :: (a, Either b c) -> Either (a, b) (a, c)
prodToSum (x, e) = 
    case e of
      Left  y -> Left  (x, y)
      Right z -> Right (x, z)
```

e aqui está aquele que vai na direção contrária:
```hs
sumToProd :: Either (a, b) (a, c) -> (a, Either b c)
sumToProd e = 
    case e of
      Left  (x, y) -> (x, Left  y)
      Right (x, z) -> (x, Right z)
```

A instrução `case of` é usada para correspondência de padrões dentro de funções. Cada padrão é seguido por uma seta e a expressão a ser avaliada quando o padrão corresponder. Por exemplo, se você chamar `prodToSum` com o valor:
```
prod1 :: (Int, Either String Float)
prod1 = (2, Left "Oi!")
```

o `e` em `case of e` de será igual à `Left "Oi!"`. Ele corresponderá ao padrão `Left y`, substituindo `"Oi!"` para `y`. Como o `x` já foi correspondido a `2`, o resultado da cláusula `case of` e de toda a função será `Left (2, "Oi!")`, Como esperado.

Não vou provar que essas duas funções são inversas uma da outra, mas se você pensar bem, elas devem ser! Eles estão apenas reembalando trivialmente o conteúdo das duas estruturas de dados. São os mesmos dados, apenas um formato diferente.

Os matemáticos têm um nome para esses dois monoides entrelaçados: é chamado de semianel. Não é um anel completo, porque não podemos definir a subtração de tipos. É por isso que um semianel às vezes é chamado de rig, que é um trocadilho com "anel sem n" (negativo). Mas, tirando isso, podemos ganhar muito ao traduzir afirmações sobre, digamos, números naturais, que formam uma estrutura, em afirmações sobre tipos. Aqui está uma tabela de tradução com algumas entradas de interesse:

| Números   | Tipos                         |
|---        |---                            |
| 0         | Void                          |
| 1         | ()                            |
| a + b     | Either a b                    |
| a * b     | (a, b) ou Pair a b = Pair a b |
| 2 = 1 + 1 | Bool                          |
| 1 + a     | Maybe                         |

O tipo de lista é bastante interessante, porque é definido como uma solução para uma equação. O tipo que estamos definindo aparece em ambos os lados da equação:
```hs
List a = Nil | Cons a (List a)
```

Se fizermos nossas substituições usuais e também substituirmos a Lista a por x, obteremos a equação:
```hs
x = 1 + a * x
```

Não podemos resolvê-lo usando métodos algébricos tradicionais porque não podemos subtrair ou dividir os tipos. Mas podemos tentar uma série de substituições, onde continuamos substituindo `x` no lado direito por `(1 + a * x)`, e usamos a propriedade distributiva. Isso leva à seguinte série:
```hs
x = 1 + a*x
x = 1 + a*(1 + a*x) = 1 + a + a*a*x
x = 1 + a + a*a*(1 + a*x) = 1 + a + a*a + a*a*a*x
...
x = 1 + a + a*a + a*a*a + a*a*a*a...
```

Terminamos com uma soma infinita de produtos (tuplas), que pode ser interpretada como: Uma lista é vazia, `1`; ou um singleton, `a`; ou um par, `a * a`; ou um triplo, `a * a * a`; etc... Bem, isso é exatamente o que uma lista é - uma sequência de `a`s!

As listas são muito mais do que isso, e voltaremos a elas e a outras estruturas de dados recursivas depois de aprendermos sobre funtores e pontos fixos.

Resolvendo equações com variáveis simbólicas - isso é álgebra! É o que dá a esses tipos seu nome: tipos de dados algébricos.

Finalmente, devo mencionar uma interpretação muito importante da álgebra de tipos. Observe que um produto de dois tipos `a` e `b` deve conter um valor do tipo `a` e um valor do tipo `b`, o que significa que ambos os tipos devem ser habitados. Uma soma de dois tipos, por outro lado, contém um valor do tipo `a` ou um valor do tipo `b`, então é suficiente se um deles for habitado. Lógico `and` e `or` também formam um semianel, e também pode ser mapeado na teoria de tipo:

| Lógica   | Tipos                           |
|---       |---                              |
| false    | Void                            |
| true     | ()                              |
| a ou b   | Either a b                      |
| a e b    | (a, b)                          |

Essa analogia é mais profunda e é a base do isomorfismo de Curry-Howard entre a lógica e a teoria dos tipos. Voltaremos a ela quando falarmos sobre os tipos de função.

#### Desafios
1. Mostre o isomorfismo entre `Maybe a` e `Either () a`.
2. Aqui está um sum type definido em Haskell:
```hs
data Shape
    = Circle Float
    | Rect Float Float
```

Quando queremos definir uma função como `area` que atua em um `Shape`, fazemos isso por correspondência de padrões nos dois construtores:
```hs
area :: Shape -> Float
area (Circle r) = pi * r * r
area (Rect d h) = d * h
```

Implemente o `Shape` em C++ ou Java como uma interface e crie duas classes: `Circle` e `Rect`. Implemente a `area` como uma função virtual.
3. Continuando com o exemplo anterior: Podemos facilmente adicionar uma nova função `circ` que calcula a circunferência de um `Shape`. Podemos fazer isso sem tocar na definição de `Shape`:
```hs
circ :: Shape -> Float
circ (Circle r) = 2.0 * pi * r
circ (Rect d h) = 2.0 * (d + h)
```

Adicione `circ` à sua implementação C++ ou Java. Que partes do código original você teve que tocar?
4. Continuando: Adicione uma nova forma, `Square`, à `Shape` e faça todas as atualizações necessárias. Que código você teve que tocar em Haskell vs. C++ ou Java? (Mesmo se você não for um programador Haskell, as modificações devem ser bastante óbvias.)
5. Mostre que `a + a = 2 * a` vale para tipos (até isomorfismo). Lembre-se de que `2` corresponde a `Bool`, de acordo com nossa tabela de tradução.

### Funtores
Correndo o risco de soar como um disco quebrado, direi o seguinte sobre funtores: Um funtor é uma ideia muito simples, mas poderosa. A teoria das categorias está repleta dessas idéias simples, mas poderosas. Um funtor é um mapeamento entre categorias. Dadas duas categorias, C e D, um funtor F mapeia objetos em C para objetos em D - é uma função em objetos. Se `a` é um objeto em C, vamos escrever sua imagem em D como `F a` (sem parênteses). Mas uma categoria não são apenas objetos - são objetos e morfismos que os conectam. Um funtor também mapeia morfismos - é uma função em morfismos. Mas não mapeia morfismos quer queira quer não - preserva conexões. Portanto, se um morfismo `f` em C conecta o objeto `a` ao objeto `b`,
```hs
f :: a -> b
```

a imagem de `f` em D, `F f`, conectará a imagem de `a` à imagem de `b`:
```
F f :: F a -> F b
```

(Esta é uma mistura de notação matemática e Haskell que espero que faça sentido agora. Não usarei parênteses ao aplicar funtores a objetos ou morfismos.)

![](images/image_13.jpg)

Como você pode ver, um funtor preserva a estrutura de uma categoria: o que está conectado em uma categoria será conectado na outra categoria. Mas há algo mais na estrutura de uma categoria: há também a composição dos morfismos. Se `h` for uma composição de `f` e `g`:
```hs
h = g . f
```

queremos que sua imagem sob F seja uma composição das imagens de `f` e `g`:
```hs
F h = F g . F f
```
![](images/image_14.jpg)

Finalmente, queremos que todos os morfismos de identidade em C sejam mapeados para morfismos de identidade em D:
$`F id_a = id_{Fa}`$

Aqui, $`id_a`$ é a identidade no objeto `a`, e $`id_{Fa}`$ é a identidade em `F a`.
![](images/image_15.jpg)

Observe que essas condições tornam os funtores muito mais restritivos do que as funções regulares. Os funtores devem preservar a estrutura de uma categoria. Se você imaginar uma categoria como uma coleção de objetos mantidos juntos por uma rede de morfismos, um funtor não tem permissão para introduzir qualquer rasgo neste tecido. Pode esmagar objetos juntos, pode colar vários morfismos em um, mas nunca pode separar as coisas. Essa restrição de não ruptura é semelhante à condição de continuidade que você pode conhecer a partir do cálculo. Nesse sentido, os funtores são “contínuos” (embora exista uma noção ainda mais restritiva de continuidade para os funtores). Assim como as funções, os funtores podem recolher e incorporar. O aspecto de incorporação é mais proeminente quando a categoria de origem é muito menor do que a categoria de destino. No extremo, a fonte pode ser a categoria singleton trivial - uma categoria com um objeto e um morfismo (a identidade). Um funtor da categoria singleton para qualquer outra categoria simplesmente seleciona um objeto nessa categoria. Isso é totalmente análogo à propriedade de morfismos de conjuntos singleton selecionando elementos em conjuntos de destino. O funtor em colapso máximo é chamado de funtor constante $`Δ_c`$. Ele mapeia cada objeto na categoria de origem para um objeto selecionado `c` na categoria de destino. Ele também mapeia cada morfismo na categoria de origem para o morfismo de identidade $`id_c`$. Ele atua como um buraco negro, compactando tudo em uma singularidade. Veremos mais sobre este funtor quando discutirmos limites e colimites.

#### Funtores em programação
Vamos começar a falar sobre programação. Temos nossa categoria de tipos e funções. Podemos falar sobre funtores que mapeiam essa categoria em si mesma - tais funtores são chamados de endofuntores. Então, o que é um endofuntor na categoria de tipos? Em primeiro lugar, ele mapeia tipos em tipos. Vimos exemplos de tais mapeamentos, talvez sem perceber que eram apenas isso. Estou falando sobre definições de tipos que foram parametrizados por outros tipos. Vejamos alguns exemplos.

##### O funtor Maybe
A definição de `Maybe` é um mapeamento do tipo `a` para o tipo `Maybe a`:
```hs
data Maybe a = Nothing | Just a
```

Aqui está uma sutileza importante: `Maybe` sozinho não é um tipo, é um construtor de tipo. Você tem que dar a ele um argumento de tipo, como `Int` ou `Bool`, para transformá-lo em um tipo. Talvez sem nenhum argumento represente uma função em tipos. Mas podemos transformar `Maybe` em um funtor? (De agora em diante, quando falo de funtores no contexto de programação, quase sempre me referirei a endofuntores.) Um funtor não é apenas um mapeamento de objetos (aqui, tipos), mas também um mapeamento de morfismos (aqui, funções). Para qualquer função de `a` a `b`:
```hs
f :: a -> b
```

gostaríamos de produzir uma função de `Maybe a` para `Maybe b`. Para definir essa função, teremos dois casos a considerar, correspondentes aos dois construtores de `Maybe`. O caso `Nothing` é simples: nós apenas retornaremos `Nothing` de volta. E se o argumento for `Just`, aplicaremos a função `f` ao seu conteúdo. Portanto, a imagem de `f` em `Maybe` é a função:
```hs
f' :: Maybe a -> Maybe b
f' Nothing  = Nothing
f' (Just x) = Just (f x)
```

(A propósito, em Haskell você pode usar apóstrofos em nomes de variáveis, o que é muito útil em casos como esses.) Em Haskell, implementamos a parte de mapeamento de morfismo de um funtor como uma função de ordem superior chamada `fmap`. No caso de `Maybe`, possui a seguinte assinatura:
```hs
fmap :: (a -> b) -> (Maybe a -> Maybe b)
```
![](images/image_16.jpg)

Costumamos dizer que `fmap` plageia uma função. A função plageada atua sobre os valores `Maybe`. Como de costume, por causa do currying, essa assinatura pode ser interpretada de duas maneiras: como uma função de um argumento - que por sua vez é uma função `(a -> b)` - retornando uma função `(Maybe a -> Maybe b)`; ou como uma função de dois argumentos retornando `Maybe b`:
```hs
fmap :: (a -> b) -> Maybe a -> Maybe b
```

Com base em nossa discussão anterior, é assim que implementamos `fmap` para `Maybe`:
```hs
fmap _ Nothing = Nothing
fmap f (Just x) = Just (f x)
```

Para mostrar que o construtor de tipo `Maybe` junto com a função `fmap` formam um funtor, temos que provar que `fmap` preserva identidade e composição. Essas são chamadas de “leis de funtor”, mas elas simplesmente garantem a preservação da estrutura da categoria.

##### Equational reasoning
Para provar as leis de funtor, usarei o raciocínio equacional, que é uma técnica de prova comum em Haskell. Ele tira vantagem do fato de que as funções de Haskell são definidas como igualdades: o lado esquerdo é igual ao lado direito. Você sempre pode substituir um pelo outro, possivelmente renomeando variáveis para evitar conflitos de nome. Pense nisso como uma função embutida ou, ao contrário, refatorando uma expressão em uma função. Vejamos a função de identidade como exemplo:
```hs
id x = x
```

Se você vir, por exemplo, `id y` em alguma expressão, você pode substituí-lo por `y` (inlining). Além disso, se você vir `id` aplicado a uma expressão, digamos `id (y + 2)`, poderá substituí-lo pela própria expressão `(y + 2)`. E essa substituição funciona nos dois sentidos: você pode substituir qualquer expressão `e` por `id e` (refatoração). Se uma função for definida por correspondência de padrões, você pode usar cada sub-definição independentemente. Por exemplo, dada a definição acima de `fmap`, você pode substituir `fmap f Nothing` por `Nothing`, ou vice-versa. Vamos ver como isso funciona na prática. Vamos começar com a preservação da identidade:
```hs
fmap id = id
```

Há dois casos a serem considerados: `Nothing` e `Just`. Aqui está o primeiro caso (estou usando o pseudocódigo Haskell para transformar o lado esquerdo para o lado direito):
```hs
  fmap id Nothing 
= { definição de fmap }
  Nothing 
= { definição de id }
  id Nothing
```

Observe que na última etapa eu usei a definição de `id` ao contrário. Substituí a expressão `Nothing` por `id Nothing`. Na prática, você realiza essas provas “queimando a vela nas duas pontas”, até atingir a mesma expressão no meio - aqui era `Nothing`. O segundo caso também é fácil:
```hs
  fmap id (Just x) 
= { definição de fmap }
  Just (id x) 
= { definição de id }
  Just x
= { definição de id }
  id (Just x)
```

Agora, vamos mostrar que o `fmap` preserva a composição:
```hs
fmap (g . f) = fmap g . fmap f
```

Primeiro o caso do `Nothing`:
```hs
  fmap (g . f) Nothing 
= { definição de fmap }
  Nothing 
= { definição de fmap }
  fmap g Nothing
= { definição de fmap }
  fmap g (fmap f Nothing)
```

E então o caso do `Just`:
```hs
  fmap (g . f) (Just x)
= { definição de fmap }
  Just ((g . f) x)
= { definição de composição }
  Just (g (f x))
= { definição de fmap }
  fmap g (Just (f x))
= { definição de fmap }
  fmap g (fmap f (Just x))
= { definição de composição }
  (fmap g . fmap f) (Just x)
```

Vale ressaltar que o raciocínio equacional não funciona para "funções" do estilo C++ com efeitos colaterais. Considere este código:
```cpp
int square(int x) {
    return x * x;
}

int counter() {
    static int c = 0;
    return c++;
}

double y = square(counter());
```

Usando o raciocínio equacional, você seria capaz de inlinear o `square` para obter:
```cpp
double y = counter() * counter();
```

Definitivamente, essa não é uma transformação válida e não produzirá o mesmo resultado. Apesar disso, o compilador C++ tentará usar o raciocínio equacional se você implementar `square` como uma macro, com resultados desastrosos.

##### Optional
Funções são facilmente expressas em Haskell, mas podem ser definidas em qualquer linguagem que suporte programação genérica e funções de ordem superior. Vamos considerar o análogo C++ de `Maybe`, o tipo de template `optional`. Aqui está um esboço da implementação (a implementação real é muito mais complexa, lidando com várias maneiras pelas quais o argumento pode ser passado, com semântica de cópia e com os problemas de gerenciamento de recursos característicos do C++):
```cpp
template<class T>
class optional {
    bool _isValid; // a tag
    T    _v;
public:
    optional()    : _isValid(false) {}         // Nothing
    optional(T x) : _isValid(true), _v(x) {}  // Just
    bool isValid() const { return _isValid; }
    T val() const { return _v; }
};
```

Esta template fornece uma parte da definição de um funtor: o mapeamento de tipos. Ele mapeia qualquer tipo `T` para um novo tipo `optional<T>`. Vamos definir sua ação nas funções:
```cpp
template<class A, class B>
std::function<optional<B>(optional<A>)> 
fmap(std::function<B(A)> f) 
{
    return [f](optional<A> opt) {
        if (!opt.isValid())
            return optional<B>{};
        else
            return optional<B>{ f(opt.val()) };
    };
}
```

Esta é uma função de ordem superior, tomando uma função como um argumento e retornando uma função. Aqui está a versão uncurried disso:
```cpp
template<class A, class B>
optional<B> fmap(std::function<B(A)> f, optional<A> opt) {
    if (!opt.isValid())
        return optional<B>{};
    else
        return optional<B>{ f(opt.val()) };
}
```

Também existe a opção de tornar o `fmap` um método template de `optional`. Esse embaraço de escolhas torna a abstração do padrão de funtor em C++ um problema. O funtor deve ser uma interface para herdar (infelizmente, você não pode ter templates de funções virtuais)? Deve ser uma função de template livre com curry ou uncurried? O compilador C++ pode inferir corretamente os tipos ausentes ou eles devem ser especificados explicitamente? Considere uma situação em que a função de entrada `f` leva um `int` para um `bool`. Como o compilador descobrirá o tipo de `g`:
```cpp
auto g = fmap(f);
```

especialmente se, no futuro, houver vários funtores sobrecarregando o `fmap`? (Veremos mais funtores em breve.)

##### Typeclasses
Então, como Haskell lida com a abstração do funtor? Ele usa o mecanismo de typeclass. Uma typeclass define uma família de tipos que oferecem suporte a uma interface comum. Por exemplo, a classe de objetos que suportam igualdade é definida da seguinte forma:
```hs
class Eq a where
    (==) :: a -> a -> Bool
```

Esta definição afirma que o tipo `a` é da classe `Eq` se suportar o operador `(==)` que recebe dois argumentos do tipo `a` e retorna um `Bool`. Se você quiser dizer a Haskell que um tipo específico é `Eq`, você deve declará-lo uma `instance` desta classe e fornecer a implementação de `(==)`. Por exemplo, dada a definição de um `Point` 2D (um tipo de produto de dois `Float`s):
```hs
data Point = Pt Float Float
```

você pode definir a igualdade de pontos:
```hs
instance Eq Point where
    (Pt x y) == (Pt x' y') = x == x' && y == y'
```

Aqui eu usei o operador `(==)` (aquele que estou definindo) na posição infixa entre os dois padrões `(Pt x y)` e `(Pt x' y')`. O corpo da função segue o único sinal de igual. Depois que `Point` é declarado uma instância da `Eq`, você pode comparar diretamente a igualdade de pontos. Observe que, ao contrário de C++ ou Java, você não precisa especificar a classe `Eq` (ou interface) ao definir `Point` - você pode fazer isso mais tarde no código do cliente. Typeclasses também são o único mecanismo de Haskell para sobrecarregar funções (e operadores). Precisamos disso para sobrecarregar o `fmap` para diferentes funtores. Porém, há uma complicação: um funtor não é definido como um tipo, mas como um mapeamento de tipos, um construtor de tipo. Precisamos de uma typeclass que não seja uma família de tipos, como era o caso da `Eq`, mas uma família de construtores de tipo. Felizmente, uma typeclass Haskell funciona tanto com construtores de tipos quanto com tipos. Então, aqui está a definição da classe `Functor`:
```hs
class Functor f where
    fmap :: (a -> b) -> f a -> f b
```

Ele estipula que `f` é um `Functor` se existir uma função `fmap` com a assinatura de tipo especificada. O `f` minúsculo é uma variável de tipo, semelhante às variáveis de tipo `a` e `b`. O compilador, entretanto, é capaz de deduzir que ele representa um construtor de tipo em vez de um tipo, observando seu uso: atuando em outros tipos, como em `f a` e `f b`. Da mesma forma, ao declarar uma instância de `Functor`, você deve fornecer a ela um construtor de tipo, como é o caso de `Maybe`:
```hs
instance Functor Maybe where
    fmap _ Nothing  = Nothing
    fmap f (Just x) = Just (f x)
```

A propósito, a classe `Functor`, bem como suas definições de instância para vários tipos de dados simples, incluindo `Maybe`, fazem parte da biblioteca Prelude padrão.

##### Functor em C++
Podemos tentar a mesma abordagem em C++? Um construtor de tipo corresponde a uma template de classe, como `optional`, portanto, por analogia, parametrizaríamos `fmap` com um parâmetro de template `F`. Esta é a sintaxe para ela:
```cpp
template<template<class> F, class A, class B>
F<B> fmap(std::function<B(A)>, F<A>);
```

Gostaríamos de especializar este modelo para diferentes funtores. Infelizmente, há uma proibição contra a especialização parcial de templates de funções de em C++. Você não pode escrever:
```cpp
template<class A, class B>
optional<B> fmap<optional>(std::function<B(A)> f, optional<A> opt)
```

Em vez disso, temos que recorrer à sobrecarga de funções, o que nos traz de volta à definição original do `fmap` uncurried:
```cpp
template<class A, class B>
optional<B> fmap(std::function<B(A)> f, optional<A> opt) {
    if (!opt.isValid())
        return optional<B>{};
    else
        return optional<B>{ f(opt.val()) };
}
```

Essa definição funciona, mas apenas porque o segundo argumento de `fmap` seleciona a sobrecarga. Ele ignora totalmente a definição mais genérica de `fmap`.

##### O funtor lista
Para ter alguma intuição quanto ao papel dos funtores na programação, precisamos examinar mais exemplos. Qualquer tipo parametrizado por outro tipo é candidato a funtor. Os contêineres genéricos são parametrizados pelo tipo dos elementos que armazenam, então, vamos dar uma olhada em um contêiner muito simples, a lista:
```hs
data List a = Nil | Cons a (List a)
```

Temos o construtor de tipo `List`, que é um mapeamento de qualquer tipo `a` para o tipo `List a`. Para mostrar que `List` é um funtor, temos que definir o plágio de funções: Dada uma função `a -> b` defina uma função `List a -> List b`:
```hs
fmap :: (a -> b) -> (List a -> List b)
```

Uma função atuando em `List a` deve considerar dois casos correspondentes aos dois construtores de lista. O caso `Nil` é trivial - basta retornar `Nil` - não há muito que você possa fazer com uma lista vazia. O caso `Cons` é um pouco complicado, porque envolve recursão. Então, vamos voltar por um momento e considerar o que estamos tentando fazer. Temos uma lista de `a`, uma função `f` que transforma `a` em `b`, e queremos gerar uma lista de `b`. O óbvio é usar `f` para transformar cada elemento da lista de `a` para `b`. Como fazemos isso na prática, dado que uma lista (não vazia) é definida como o `Cons` de uma cabeça e uma cauda? Aplicamos `f` à cabeça e aplicamos `f` plagiando (`fmap`ped) à cauda. Esta é uma definição recursiva, porque estamos definindo `f` plagiado em termos de `f` plagiado:
```hs
fmap f (Cons x t) = Cons (f x) (fmap f t)
```

Observe que, no lado direito, `fmap f` é aplicado a uma lista que é mais curta do que a lista para a qual estamos definindo - é aplicado a sua cauda. Recursamos em direção a listas cada vez mais curtas, de modo que eventualmente chegaremos à lista vazia, ou `Nil`. Mas, como decidimos anteriormente, `fmap f` agindo em `Nil` retorna `Nil`, encerrando assim a recursão. Para obter o resultado final, combinamos a nova cabeça `(f x)` com a nova cauda `(fmap f t)` usando o construtor `Cons`. Juntando tudo, aqui está a declaração de instância para o funtor de lista:
```hs
instance Functor List where
    fmap _ Nil = Nil
    fmap f (Cons x t) = Cons (f x) (fmap f t)
```

Se você está mais confortável com C++, considere o caso de um `std::vector`, que pode ser considerado o contêiner C++ mais genérico. A implementação de `fmap` para `std::vector` é apenas um encapsulamento fino de `std::transform`:
```cpp
template<class A, class B>
std::vector<B> fmap(std::function<B(A)> f, std::vector<A> v) {
    std::vector<B> w;
    std::transform( std::begin(v)
                  , std::end(v)
                  , std::back_inserter(w)
                  , f);
    return w;
}
```

Podemos usá-lo, por exemplo, para elevar ao quadrado os elementos de uma sequência de números:
```cpp
std::vector<int> v{ 1, 2, 3, 4 };
auto w = fmap([](int i) { return i*i; }, v);
std::copy(std::begin(w)
         , std::end(w)
         , std::ostream_iterator(std::cout, ", "));
```

A maioria dos contêineres C++ são functores em virtude da implementação de iteradores que podem ser passados para `std::transform`, que é o primo mais primitivo de `fmap`. Infelizmente, a simplicidade de um funtor é perdida na confusão usual de iteradores e temporários (veja a implementação do `fmap` acima). Estou feliz em dizer que a nova proposta de biblioteca range de C++ torna a natureza funcional dos intervalos muito mais pronunciada.

##### O funtor reader
Agora que você deve ter desenvolvido algumas intuições - por exemplo, os functores são alguns tipos de contêineres - deixe-me mostrar um exemplo que à primeira vista parece muito diferente. Considere um mapeamento do tipo `a` para o tipo de função que retorna `a`. Nós realmente não falamos sobre os tipos de função em profundidade - o tratamento categórico completo está chegando - mas temos algum entendimento sobre eles como programadores. Em Haskell, um tipo de função é construído usando o construtor de tipo de seta `(->)` que aceita dois tipos: o tipo de argumento e o tipo de resultado. Você já o viu na forma de infixo, `a -> b`, mas pode igualmente ser usado na forma de prefixo, quando entre parênteses:
```hs
(->) a b
```

Assim como com funções regulares, funções de tipo de mais de um argumento podem ser parcialmente aplicadas. Portanto, quando fornecemos apenas um argumento de tipo para a seta, ele ainda espera outro. É por isso:
```hs
(->) a
```

é um construtor de tipo. Ele precisa de mais um tipo `b` para produzir um tipo completo `a -> b`. Como está, ele define uma família inteira de construtores de tipo parametrizados por `a`. Vamos ver se esta também é uma família de funtores. Lidar com dois parâmetros de tipo pode ficar um pouco confuso, então vamos renomear. Vamos chamar o tipo de argumento `r` e o tipo de resultado `a`, de acordo com nossas definições de funtor anteriores. Portanto, nosso construtor de tipo pega qualquer tipo `a` e mapeia-o no tipo `r -> a`. Para mostrar que é um funtor, queremos plagiar uma função `a -> b` para uma função que recebe `r -> a` e retorna `r -> b`. Esses são os tipos que são formados usando o construtor de tipo `(->) r` atuando em, respectivamente, `a` e `b`. Aqui está a assinatura de tipo de `fmap` aplicada a este caso:
```hs
fmap :: (a -> b) -> (r -> a) -> (r -> b)
```

Temos que resolver o seguinte quebra-cabeça: dada uma função `f :: a -> b` e uma função `g :: r -> a`, crie uma função `r -> b`. Só existe uma maneira de compor as duas funções e o resultado é exatamente o que precisamos. Então, aqui está a implementação de nosso `fmap`:
```hs
instance Functor ((->) r) where
    fmap f g = f . g
```

Simplesmente funciona! Se você gosta de notação concisa, esta definição pode ser reduzida ainda mais observando que a composição pode ser reescrita na forma de prefixo:
```hs
fmap f g = (.) f g
```

e os argumentos podem ser omitidos para produzir uma igualdade direta de duas funções:
```hs
fmap = (.)
```

Esta combinação do construtor de tipo `(->) r` com a implementação acima de `fmap` é chamada de funtor reader.

##### Funtores como contêineres
Vimos alguns exemplos de funtores em linguagens de programação que definem contêineres de uso geral, ou pelo menos objetos que contêm algum valor do tipo sobre o qual são parametrizados. O funtor reader parece ser um outlier, porque não pensamos em funções como dados. Mas vimos que funções puras podem ser memorizadas e a execução da função pode ser transformada em pesquisa de tabela. As tabelas são dados. Por outro lado, por causa do laziness de Haskell, um contêiner tradicional, como uma lista, pode realmente ser implementado como uma função. Considere, por exemplo, uma lista infinita de números naturais, que podem ser definidos compactamente como:
```hs
nats :: [Integer]
nats = [1..]
```

Na primeira linha, um par de colchetes é o construtor de tipo embutido de Haskell para listas. Na segunda linha, colchetes são usados ​​para criar um literal de lista. Obviamente, uma lista infinita como essa não pode ser armazenada na memória. O compilador o implementa como uma função que gera `Integer`s sob demanda. Haskell efetivamente confunde a distinção entre dados e código. Uma lista pode ser considerada uma função e uma função pode ser considerada uma tabela que mapeia argumentos para resultados. Este último pode até ser prático se o domínio da função for finito e não muito grande. Não seria prático, entretanto, implementar `strlen` como tabela de consulta, porque existem infinitas cadeias de caracteres diferentes. Como programadores, não gostamos de infinitos, mas na teoria das categorias você aprende a comer infinitos no café da manhã. Quer seja um conjunto de todas as strings ou uma coleção de todos os estados possíveis do Universo, passado, presente e futuro - podemos lidar com isso! Assim, gosto de pensar no objeto funtor (um objeto do tipo gerado por um endofuntor) como contendo um valor ou valores do tipo sobre o qual ele é parametrizado, mesmo que esses valores não estejam fisicamente presentes lá. Um exemplo de funtor é o `std::future` de C++, que pode em algum ponto conter um valor, mas não é garantido que irá; e se você quiser acessá-lo, você pode bloquear a espera por outra thread para terminar a execução. Outro exemplo é o objeto `IO` de Haskell, que pode conter a entrada do usuário ou as versões futuras do nosso Universo com “Hello World!” exibida no monitor. De acordo com esta interpretação, um objeto funtor é algo que pode conter um valor ou valores do tipo em que é parametrizado. Ou pode conter uma receita para gerar esses valores. Não estamos preocupados em poder acessar os valores - isso é totalmente opcional e está fora do escopo do funtor. Tudo o que nos interessa é ser capaz de manipular esses valores usando funções. Se os valores podem ser acessados, então devemos ser capazes de ver os resultados dessa manipulação. Se não puderem, então tudo o que importa é que as manipulações sejam compostas corretamente e que a manipulação com uma função de identidade não mude nada. Só para mostrar o quanto não nos importamos em poder acessar os valores dentro de um objeto funtor, aqui está um construtor de tipo que ignora completamente seu argumento `a`:
```hs
data Const c a = Const c
```

O construtor de tipo `Const` aceita dois tipos, `c` e `a`. Assim como fizemos com o construtor de seta, vamos aplicá-lo parcialmente para criar um funtor. O construtor de dados (também chamado de Const) assume apenas um valor do tipo `c`. Não tem dependência de `a`. O tipo de `fmap` para este construtor de tipo é:
```hs
instance Functor (Const c) where
    fmap _ (Const v) = Const v
```

Isso pode ficar um pouco mais claro em C++ (nunca pensei que diria essas palavras!), Onde há uma distinção mais forte entre argumentos de tipo - que são em tempo de compilação [compile-time] - e valores, que são em tempo de execução [runtime]:
```cpp
template<class C, class A>
struct Const {
    Const(C v) : _v(v) {}
    C _v;
};
```

A implementação de `fmap` em C++ também ignora o argumento da função e essencialmente converte novamente o argumento `Const` sem alterar seu valor:
```cpp
template<class C, class A, class B>
Const<C, B> fmap(std::function<B(A)> f, Const<C, A> c) {
    return Const<C, B>{c._v};
}
```

Apesar de sua estranheza, o funtor `Const` desempenha um papel importante em muitas construções. Na teoria das categorias, é um caso especial do funtor $`Δ_c`$ que mencionei anteriormente - o caso endofuntor de um buraco negro. Veremos mais disso no futuro.

##### Composição de funtores
Não é difícil se convencer de que os funtores entre as categorias se compõem, assim como as funções entre os conjuntos se compõem. Uma composição de dois functores, ao atuar sobre objetos, é apenas a composição de seus respectivos mapeamentos de objetos; e da mesma forma quando atuando sobre morfismos. Depois de saltar através de dois funtores, os morfismos de identidade terminam como morfismos de identidade e as composições de morfismos terminam como composições de morfismos. Não há realmente muito nisso. Em particular, é fácil compor endofuntores. Lembra da função `MaybeTail`? Vou reescrever usando a implementação de listas de Haskell:
```hs
maybeTail :: [a] -> Maybe [a]
maybeTail [] = Nothing
maybeTail (x:xs) = Just xs
```

(O construtor de lista vazia que usamos para chamar `Nil` é substituído pelo par vazio de colchetes `[]`. O construtor `Cons` é substituído pelo operador infixo `:` (dois pontos).) O resultado de `MaybeTail` é de um tipo que é uma composição de dois funtores, `Maybe` e `[]`, atuando em `a`. Cada um desses funtores está equipado com sua própria versão de `fmap`, mas e se quisermos aplicar alguma função `f` ao conteúdo do composto: uma lista `Maybe`? Precisamos romper duas camadas de funtores. Podemos usar o `fmap` para romper o `Maybe` externo. Mas não podemos simplesmente enviar `f` para dentro `Maybe` porque `f` não funcione em listas. Temos que enviar `(fmap f)` para operar na lista interna. Por exemplo, vamos ver como podemos elevar ao quadrado os elementos de uma lista `Maybe` de inteiros:
```hs
square x = x * x

mis :: Maybe [Int]
mis = Just [1, 2, 3]

mis2 = fmap (fmap square) mis
```

O compilador, após analisar os tipos, descobrirá que, para o `fmap` externo, deve usar a implementação da instância `Maybe`, e para a interna, a implementação do funtor lista. Pode não ser imediatamente óbvio que o código acima pode ser reescrito como:
```hs
mis2 = (fmap . fmap) square mis
```

Mas lembre-se de que `fmap` pode ser considerado uma função de apenas um argumento:
```hs
fmap :: (a -> b) -> (f a -> f b)
```

Em nosso caso, o segundo `fmap` em `(fmap . fmap)` leva como argumento:
```hs
square :: Int -> Int
```

e retorna uma função do tipo:
```hs
[Int] -> [Int]
```

O primeiro `fmap` então assume essa função e retorna uma função:
```hs
Maybe [Int] -> Maybe [Int]
```

Finalmente, essa função é aplicada a `mis`. Portanto, a composição de dois funtores é um funtor cujo `fmap` é a composição dos `fmap`s correspondentes. Voltando à teoria das categorias: é bastante óbvio que a composição do funtor é associativa (o mapeamento de objetos é associativo e o mapeamento de morfismos é associativo). E há também um funtor de identidade trivial em cada categoria: ele mapeia cada objeto para si mesmo, e cada morfismo para si mesmo. Portanto, os funtores têm todas as mesmas propriedades que os morfismos em alguma categoria. Mas que categoria seria essa? Teria que ser uma categoria na qual os objetos são categorias e os morfismos são funtores. É uma categoria de categorias. Mas uma categoria de todas as categorias teria de se incluir, e entraríamos nos mesmos tipos de paradoxos que tornavam o conjunto de todos os conjuntos impossível. Existe, no entanto, uma categoria de todas as categorias pequenas chamada `Cat` (que é grande, então não pode ser um membro de si mesmo). Uma categoria pequena é aquela em que os objetos formam um conjunto, em oposição a algo maior do que um conjunto. Veja bem, na teoria das categorias, mesmo um conjunto infinito e incontável é considerado "pequeno". Pensei em mencionar essas coisas porque acho bastante surpreendente que possamos reconhecer as mesmas estruturas se repetindo em muitos níveis de abstração. Veremos mais tarde que os funtores também formam categorias.

#### Desafios
1. Podemos transformar o construtor do tipo `Maybe` em um funtor definindo:
```hs
fmap _ _ = Nothing
```
que ignora ambos os seus argumentos? (Dica: verifique as leis do functor.)
2. Prove as leis do funtor para o funtor reader. Dica: é muito simples.
3. Implemente o funtor reader em sua segunda linguagem favorita (a primeira sendo Haskell, é claro).
4. Prove as leis do funtor para o funtor lista.
5. Suponha que as leis sejam verdadeiras para a parte final da lista à qual você está aplicando (em outras palavras, use a indução).

#### Agradecimentos
Gershom Bazerman é gentil o suficiente para continuar revisando essas postagens. Sou grato por sua paciência e visão.